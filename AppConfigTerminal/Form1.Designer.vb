﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CrearBDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CrearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegenerarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ABCDCONFIGURARToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SocketToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NUMERICCONFIGURARToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TestToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Test2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(751, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MenuToolStripMenuItem
        '
        Me.MenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TestToolStripMenuItem, Me.CrearBDToolStripMenuItem, Me.ABCDCONFIGURARToolStripMenuItem, Me.SocketToolStripMenuItem, Me.NUMERICCONFIGURARToolStripMenuItem, Me.TestToolStripMenuItem1, Me.Test2ToolStripMenuItem})
        Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
        Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
        Me.MenuToolStripMenuItem.Text = "Menu"
        '
        'TestToolStripMenuItem
        '
        Me.TestToolStripMenuItem.Name = "TestToolStripMenuItem"
        Me.TestToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.TestToolStripMenuItem.Text = "Test"
        '
        'CrearBDToolStripMenuItem
        '
        Me.CrearBDToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CrearToolStripMenuItem, Me.EliminarToolStripMenuItem, Me.RegenerarToolStripMenuItem})
        Me.CrearBDToolStripMenuItem.Name = "CrearBDToolStripMenuItem"
        Me.CrearBDToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.CrearBDToolStripMenuItem.Text = "Gestionar DB"
        '
        'CrearToolStripMenuItem
        '
        Me.CrearToolStripMenuItem.Name = "CrearToolStripMenuItem"
        Me.CrearToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me.CrearToolStripMenuItem.Text = "Crear"
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me.EliminarToolStripMenuItem.Text = "Eliminar"
        '
        'RegenerarToolStripMenuItem
        '
        Me.RegenerarToolStripMenuItem.Name = "RegenerarToolStripMenuItem"
        Me.RegenerarToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me.RegenerarToolStripMenuItem.Text = "Regenerar"
        '
        'ABCDCONFIGURARToolStripMenuItem
        '
        Me.ABCDCONFIGURARToolStripMenuItem.Name = "ABCDCONFIGURARToolStripMenuItem"
        Me.ABCDCONFIGURARToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.ABCDCONFIGURARToolStripMenuItem.Text = "A-B-C-D CONFIGURAR"
        '
        'SocketToolStripMenuItem
        '
        Me.SocketToolStripMenuItem.Name = "SocketToolStripMenuItem"
        Me.SocketToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.SocketToolStripMenuItem.Text = "Socket "
        '
        'NUMERICCONFIGURARToolStripMenuItem
        '
        Me.NUMERICCONFIGURARToolStripMenuItem.Name = "NUMERICCONFIGURARToolStripMenuItem"
        Me.NUMERICCONFIGURARToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.NUMERICCONFIGURARToolStripMenuItem.Text = "NUMERIC CONFIGURAR"
        '
        'TestToolStripMenuItem1
        '
        Me.TestToolStripMenuItem1.Name = "TestToolStripMenuItem1"
        Me.TestToolStripMenuItem1.Size = New System.Drawing.Size(202, 22)
        Me.TestToolStripMenuItem1.Text = "Grupos"
        '
        'Test2ToolStripMenuItem
        '
        Me.Test2ToolStripMenuItem.Name = "Test2ToolStripMenuItem"
        Me.Test2ToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.Test2ToolStripMenuItem.Text = "Estaciones"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(751, 477)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PuntoGas Sync"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents MenuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TestToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CrearBDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ABCDCONFIGURARToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SocketToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NUMERICCONFIGURARToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CrearToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RegenerarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TestToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents Test2ToolStripMenuItem As ToolStripMenuItem
End Class
