﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmFunNumeric
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button9 = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ButtonA = New System.Windows.Forms.ComboBox()
        Me.ButtonB = New System.Windows.Forms.ComboBox()
        Me.ButtonC = New System.Windows.Forms.ComboBox()
        Me.ButtonD = New System.Windows.Forms.ComboBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button9
        '
        Me.Button9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Button9.FormattingEnabled = True
        Me.Button9.Location = New System.Drawing.Point(428, 152)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(206, 21)
        Me.Button9.TabIndex = 61
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(488, 129)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(78, 20)
        Me.Label9.TabIndex = 60
        Me.Label9.Text = "Button 9"
        '
        'Button8
        '
        Me.Button8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Button8.FormattingEnabled = True
        Me.Button8.Location = New System.Drawing.Point(216, 152)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(206, 21)
        Me.Button8.TabIndex = 59
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(270, 129)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(78, 20)
        Me.Label8.TabIndex = 58
        Me.Label8.Text = "Button 8"
        '
        'Button7
        '
        Me.Button7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Button7.FormattingEnabled = True
        Me.Button7.Location = New System.Drawing.Point(4, 152)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(206, 21)
        Me.Button7.TabIndex = 57
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(66, 129)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 20)
        Me.Label7.TabIndex = 56
        Me.Label7.Text = "Button 7"
        '
        'Button5
        '
        Me.Button5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Button5.FormattingEnabled = True
        Me.Button5.Location = New System.Drawing.Point(216, 96)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(206, 21)
        Me.Button5.TabIndex = 55
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(270, 73)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 20)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "Button 5"
        '
        'Button6
        '
        Me.Button6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Button6.FormattingEnabled = True
        Me.Button6.Location = New System.Drawing.Point(428, 96)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(206, 21)
        Me.Button6.TabIndex = 53
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(488, 73)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 20)
        Me.Label5.TabIndex = 52
        Me.Label5.Text = "Button 6"
        '
        'Button4
        '
        Me.Button4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Button4.FormattingEnabled = True
        Me.Button4.Location = New System.Drawing.Point(4, 96)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(206, 21)
        Me.Button4.TabIndex = 51
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(66, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 20)
        Me.Label4.TabIndex = 50
        Me.Label4.Text = "Button 4"
        '
        'Button3
        '
        Me.Button3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Button3.FormattingEnabled = True
        Me.Button3.Location = New System.Drawing.Point(428, 41)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(206, 21)
        Me.Button3.TabIndex = 49
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(488, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 20)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Button 3"
        '
        'Button2
        '
        Me.Button2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Button2.FormattingEnabled = True
        Me.Button2.Location = New System.Drawing.Point(216, 41)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(206, 21)
        Me.Button2.TabIndex = 47
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(270, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 20)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Button 2"
        '
        'Button1
        '
        Me.Button1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Button1.FormattingEnabled = True
        Me.Button1.Location = New System.Drawing.Point(4, 41)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(206, 21)
        Me.Button1.TabIndex = 45
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(66, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 20)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Button 1"
        '
        'btnSave
        '
        Me.btnSave.BackgroundImage = Global.AppConfigTerminal.My.Resources.Resources._1450392123_save
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSave.Location = New System.Drawing.Point(16, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(49, 49)
        Me.btnSave.TabIndex = 62
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.AppConfigTerminal.My.Resources.Resources.kp_vx520
        Me.PictureBox1.Location = New System.Drawing.Point(692, 119)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(279, 301)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 43
        Me.PictureBox1.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Button9)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Button8)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Button7)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.Button4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Button6)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 218)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(640, 202)
        Me.GroupBox1.TabIndex = 63
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "PIN PAD"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ButtonD)
        Me.GroupBox2.Controls.Add(Me.ButtonC)
        Me.GroupBox2.Controls.Add(Me.ButtonB)
        Me.GroupBox2.Controls.Add(Me.ButtonA)
        Me.GroupBox2.Location = New System.Drawing.Point(20, 90)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(636, 100)
        Me.GroupBox2.TabIndex = 64
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "A-B-C-D"
        '
        'ButtonA
        '
        Me.ButtonA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ButtonA.DropDownWidth = 200
        Me.ButtonA.FormattingEnabled = True
        Me.ButtonA.Location = New System.Drawing.Point(32, 47)
        Me.ButtonA.Name = "ButtonA"
        Me.ButtonA.Size = New System.Drawing.Size(140, 21)
        Me.ButtonA.TabIndex = 62
        '
        'ButtonB
        '
        Me.ButtonB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ButtonB.FormattingEnabled = True
        Me.ButtonB.Location = New System.Drawing.Point(178, 47)
        Me.ButtonB.Name = "ButtonB"
        Me.ButtonB.Size = New System.Drawing.Size(140, 21)
        Me.ButtonB.TabIndex = 63
        '
        'ButtonC
        '
        Me.ButtonC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ButtonC.FormattingEnabled = True
        Me.ButtonC.Location = New System.Drawing.Point(324, 47)
        Me.ButtonC.Name = "ButtonC"
        Me.ButtonC.Size = New System.Drawing.Size(140, 21)
        Me.ButtonC.TabIndex = 64
        '
        'ButtonD
        '
        Me.ButtonD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ButtonD.FormattingEnabled = True
        Me.ButtonD.Location = New System.Drawing.Point(470, 47)
        Me.ButtonD.Name = "ButtonD"
        Me.ButtonD.Size = New System.Drawing.Size(140, 21)
        Me.ButtonD.TabIndex = 65
        '
        'FrmFunNumeric
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(998, 432)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmFunNumeric"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Funcionalidad PIN PAD"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button9 As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents ButtonD As ComboBox
    Friend WithEvents ButtonC As ComboBox
    Friend WithEvents ButtonB As ComboBox
    Friend WithEvents ButtonA As ComboBox
End Class
