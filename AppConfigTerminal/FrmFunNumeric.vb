﻿Imports LibUtil
Imports LibUtil.Models

Public Class FrmFunNumeric

    Dim numeric As New List(Of NumericFunctions)
    Dim full_numeric As New List(Of FullNumericFunctions)
    Dim stationId As Integer

    Public Sub New(ByVal stationId As Integer)
        InitializeComponent()
        Me.stationId = stationId

    End Sub

    Private Sub fillbutton()
        Dim dv As New DropValues
        'Dim p = dv.getTerminalNumbers
        'Dim m = p.Find(Function(x) x.Value.Contains("A"))

        ButtonA.ValueMember = "Value"
        ButtonA.DisplayMember = "Text"
        ButtonA.DataSource = dv.getTerminalNumbers

        ButtonB.ValueMember = "Value"
        ButtonB.DisplayMember = "Text"
        ButtonB.DataSource = dv.getTerminalNumbers

        ButtonC.ValueMember = "Value"
        ButtonC.DisplayMember = "Text"
        ButtonC.DataSource = dv.getTerminalNumbers

        ButtonD.ValueMember = "Value"
        ButtonD.DisplayMember = "Text"
        ButtonD.DataSource = dv.getTerminalNumbers

        Button1.ValueMember = "Value"
        Button1.DisplayMember = "Text"
        Button1.DataSource = dv.getTerminalNumbers

        'Button1.DataSource = CargarBaseDrop.Tables(0)

        Button2.ValueMember = "Value"
        Button2.DisplayMember = "Text"
        Button2.DataSource = dv.getTerminalNumbers

        Button3.ValueMember = "Value"
        Button3.DisplayMember = "Text"
        Button3.DataSource = dv.getTerminalNumbers

        Button4.ValueMember = "Value"
        Button4.DisplayMember = "Text"
        Button4.DataSource = dv.getTerminalNumbers

        Button5.ValueMember = "Value"
        Button5.DisplayMember = "Text"
        Button5.DataSource = dv.getTerminalNumbers

        Button6.ValueMember = "Value"
        Button6.DisplayMember = "Text"
        Button6.DataSource = dv.getTerminalNumbers

        Button7.ValueMember = "Value"
        Button7.DisplayMember = "Text"
        Button7.DataSource = dv.getTerminalNumbers

        Button8.ValueMember = "Value"
        Button8.DisplayMember = "Text"
        Button8.DataSource = dv.getTerminalNumbers

        Button9.ValueMember = "Value"
        Button9.DisplayMember = "Text"
        Button9.DataSource = dv.getTerminalNumbers

    End Sub


    Private Sub SelectOptionDrop()
        Dim Ds As New DataSet
        Dim ct As Integer
        Ds = CargarBaseDropNumeric()
        Dim numeric_functions As New NumericFunctions
        full_numeric = numeric_functions.getNF(1)
        If full_numeric.Count = 0 Then
            Exit Sub
        End If
        ct = 0
        Dim g = full_numeric.ElementAt(0).DropValuesValue
        For value As Integer = 0 To 12
            Select Case ct
                Case 0
                    Button1.SelectedValue = full_numeric.ElementAt(0).DropValuesValue
                Case 1
                    Button2.SelectedValue = full_numeric.ElementAt(1).DropValuesValue
                Case 2
                    Button3.SelectedValue = full_numeric.ElementAt(2).DropValuesValue
                Case 3
                    Button4.SelectedValue = full_numeric.ElementAt(3).DropValuesValue
                Case 4
                    Button5.SelectedValue = full_numeric.ElementAt(4).DropValuesValue
                Case 5
                    Button6.SelectedValue = full_numeric.ElementAt(5).DropValuesValue
                Case 6
                    Button7.SelectedValue = full_numeric.ElementAt(6).DropValuesValue
                Case 7
                    Button8.SelectedValue = full_numeric.ElementAt(7).DropValuesValue
                Case 8
                    Button9.SelectedValue = full_numeric.ElementAt(8).DropValuesValue
                Case 9
                    ButtonA.SelectedValue = full_numeric.ElementAt(9).DropValuesValue
                Case 10
                    ButtonB.SelectedValue = full_numeric.ElementAt(10).DropValuesValue
                Case 11
                    ButtonC.SelectedValue = full_numeric.ElementAt(11).DropValuesValue
                Case 12
                    ButtonD.SelectedValue = full_numeric.ElementAt(12).DropValuesValue
            End Select
            ct = ct + 1

        Next

    End Sub

    Private Sub FrmFunNumeric_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fillbutton()
        SelectOptionDrop()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SvButton As New ClsUtil
        Dim DsNumeric As DataSet
        Dim list As New List(Of String)
        list.Add(Button1.SelectedValue)
        list.Add(Button2.SelectedValue)
        list.Add(Button3.SelectedValue)
        list.Add(Button4.SelectedValue)
        list.Add(Button5.SelectedValue)
        list.Add(Button6.SelectedValue)
        list.Add(Button7.SelectedValue)
        list.Add(Button8.SelectedValue)
        list.Add(Button9.SelectedValue)

        list.Add(ButtonA.SelectedValue)
        list.Add(ButtonB.SelectedValue)
        list.Add(ButtonC.SelectedValue)
        list.Add(ButtonD.SelectedValue)

        Dim btn_listValues = SvButton.FunctionNumeric(list)
        Dim terminal_numbers As New TerminalNumber
        Dim list_numbers = terminal_numbers.getTerminalNumbers()
        'Dim numeric As New List(Of NumericFunctions)
        Dim numeric_functions As New NumericFunctions
        numeric = numeric_functions.getNF2(1)


        For value As Integer = 0 To 12
            If numeric.Count < 13 Then
                Dim n As New NumericFunctions
                n.DropValuesId = btn_listValues.ElementAt(value).Id
                n.TerminalNumberId = list_numbers.ElementAt(value).Id
                n.StationId = Me.stationId
                numeric.Add(n)
            Else
                numeric.ElementAt(value).DropValuesId = btn_listValues.ElementAt(value).Id
                numeric.ElementAt(value).TerminalNumberId = list_numbers.ElementAt(value).Id
                numeric.ElementAt(value).StationId = Me.stationId
            End If

        Next


        'DsNumeric = SvButton.FunctionNumeric("XmlNUMERIC", Button1.SelectedValue, Button2.SelectedValue,
        '                               Button3.SelectedValue, Button4.SelectedValue,
        '                               Button5.SelectedValue, Button6.SelectedValue,
        '                               Button7.SelectedValue, Button8.SelectedValue,
        '                               Button9.SelectedValue)
        'SvButton.WriteDataXml(NameXmlNUMERIC, DsNumeric, SvButton.GetPath)
        Dim h = numeric_functions.insertNF(numeric)
        SelectOptionDrop()
    End Sub

    Private Sub Button1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Button1.SelectedIndexChanged

    End Sub
End Class