﻿Imports System.Threading
Imports System.Globalization
Public Class FrmSocketSend
    Dim WithEvents WinSockServer As New ClsServer

    Dim servidor As String
    Dim basedatos As String
    Dim usuario As String
    Dim pass As String
    Dim port As String

    Private Function readxml() As Integer
        Dim dataTable As DataTable
        Dim dataset As New DataSet
        Dim dt As New DataSet
        Dim strpath As String = My.Application.Info.DirectoryPath

        Try
            Dim pathfull As String = strpath & "\config.xml"
            dataset.Clear()

            For Each dataTable In dataset.Tables
                dataTable.BeginLoadData()
            Next

            dataset.ReadXml(pathfull)

            For Each dataTable In dataset.Tables
                dataTable.EndLoadData()
            Next
            'dataset.Tables(0).Select(String.Format("bomba={0}", B), "bomba ASC")

            For Each rd As DataRow In dataset.Tables(0).Rows
                txtport.Text = rd("Puerto")
                servidor = rd("Servidor")
                basedatos = rd("BaseDatos")
                usuario = rd("Usuario")
                pass = rd("Contraseña")
                port = rd("PuertoDb")


            Next
            Return 0
        Catch ex As Exception
            'Regis.escribirficheroInfoinicio(String.Concat("ReadXML ", "  |  ", ex.Message, "|", Now))
            Return 1
        End Try
        'WindowsEventLog.CreateError(strpath, 20)

    End Function

    Private Sub FrmSocketSend_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")
        CheckForIllegalCrossThreadCalls = False

        


        If readxml() = 0 Then

            With WinSockServer
                'Establezco el puerto donde escuchar
                .PuertoDeEscucha = Me.txtport.Text

                'Comienzo la escucha
                .Escuchar()
            End With
        Else
            MsgBox("Ingrese al modulo de configuracion,para establecer parametros de inicio")
            'Regis.escribirficheroInfoinicio(String.Concat("Dashboard load ", "  |  ", "Ingrese al modulo de configuracion, para establecer parametros de inicio"))
        End If
    End Sub

    Private Sub WinSockServer_ConexionTerminada(IDTerminal As Net.IPEndPoint) Handles WinSockServer.ConexionTerminada

    End Sub


    Private Function OptionABCD() As String
        Dim Ds As New DataSet

        Dim stringConcat As String = Nothing
        Dim i As Integer = 0
        Ds = CargarBaseDropABCD()

        If Ds Is Nothing Then
            Exit Function
        End If

        For Each row As DataRow In Ds.Tables(0).Rows
            If i = 0 Then
                stringConcat = String.Concat(stringConcat, "A", row("Value"))
            Else
                stringConcat = String.Concat(stringConcat, "^", row("Value"))
            End If
            i = i + 1
        Next

        Return stringConcat
    End Function

    Private Function OptionNumeric() As String
        Dim Ds As New DataSet

        Dim stringConcat As String = Nothing
        Dim i As Integer = 0
        Ds = CargarBaseDropNumeric()

        If Ds Is Nothing Then
            Exit Function
        End If

        For Each row As DataRow In Ds.Tables(0).Rows
            If i = 0 Then
                stringConcat = String.Concat(stringConcat, "Z", row("Value"))
            Else
                stringConcat = String.Concat(stringConcat, "^", row("Value"))
            End If
            i = i + 1
        Next

        Return stringConcat
    End Function


    Private Sub WinSockServer_DatosRecibidos(IDTerminal As Net.IPEndPoint) Handles WinSockServer.DatosRecibidos
        Dim strdatos As String
        Dim objListItem As New ListViewItem
        strdatos = WinSockServer.ObtenerDatos(IDTerminal)
        Dim respuesta As String

        Dim str As String = Nothing REM cadena a parsear
        Dim strsininifin As String = Nothing REM cadena limpia sin inicio y fin
        Dim strsininifin1 As String = Nothing REM cadena limpia sin inicio y fin
        Dim cadena() As String

        Try
            str = strdatos.Substring(4)

            REM Regis.escribirfichero(String.Concat(" Receive GPS ", "  |  ", strdatos, "  |  ", Now()))
            ' Regis.escribirficheroInformacioEntrante(String.Concat(" Receive GPS ", "  |  ", strdatos, "  |  ", Now()))

            strsininifin = str.Replace(">", "")
            strsininifin1 = strsininifin.Replace("<", "")

            str = Nothing

            str = strsininifin1

            cadena = str.Split("=")
        Catch ex As Exception
            ' Regis.escribirfichero(String.Concat("Lista ", "  |  ", strdatos, "  |  ", ex.Message, "  |  ", Now()))
            WinSockServer.EnviarDatos(CType(IDTerminal, System.Net.IPEndPoint), String.Format("*{0}{1}", Format(3 + 3, "00#"), "U=5"))
            Exit Sub
        End Try



        Try
            'BackgroundWorker1.RunWorkerAsync()

            For i As Integer = 0 To ListView1.Items.Count - 1
                If ListView1.Items(i).SubItems(2).Text = IDTerminal.Address.ToString And CInt(ListView1.Items(i).SubItems(3).Text) = CInt(IDTerminal.Port) Then
                    Me.ListView1.Items(i).SubItems(1).Text = (cadena(0))
                    Me.ListView1.Items(i).SubItems(4).Text = (strdatos)
                    Me.ListView1.Items(i).SubItems(5).Text = Now.ToString
                End If
            Next
        Catch ex As Exception
            ' Regis.escribirfichero(String.Concat("Lista ", "  |  ", strdatos, "  |  ", "Error al mostrar informacion en listview1", "  |  ", Now()))
        End Try


        Try

            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-MX")

            'Comprobar que llegaron todos los datos
            Dim largo As Integer
            'Dim Estructura As ClsVentas
            'Dim d As String = datos.Trim
            'WindowsEventLog.CreateWarning(String.Concat(NombreAplicacion, datos), 827)


            largo = CInt(Val(strdatos.Substring(0, 3)))

            REM WindowsEventLog.CreateWarning(datos, 672)

            If largo <> strdatos.Trim.Length Then


                WinSockServer.EnviarDatos(CType(IDTerminal, System.Net.IPEndPoint), String.Format("*{0}{1}", Format(3 + 3, "00#"), "U=5"))
                
                Exit Sub
            End If

        Catch ex As Exception

            WinSockServer.EnviarDatos(CType(IDTerminal, System.Net.IPEndPoint), String.Format("*{0}{1}", Format(3 + 3, "00#"), "U=5"))
            
            Exit Sub
        End Try

        Try


            Select Case strdatos.Substring(3, 1)



                Case "A" 'Funciones primarias A-B-C-D
                    Dim str1 As String = OptionABCD()
                    respuesta = String.Format("{0}{1}", Format(3 + String.Concat(str1, "^", "").Length, "00#"), String.Concat(str1, "^", ""))
                Case "Z" REM Funciones secundarias terminal
                    Dim str1 As String = OptionNumeric()
                    respuesta = String.Format("{0}{1}", Format(3 + String.Concat(str1, "^", "").Length, "00#"), String.Concat(str1, "^", ""))
                Case "M" REM MOTOR                  
                    'StatusMotorUpdate(strdatos, respuesta, IDTerminal) 'procesa la informacion
                Case "P" REM respuesta desde el socket
                    'UbicacionPeticion2(strdatos, respuesta, IDTerminal) 'procesa la informacion
                Case "S" REM Peticion desde el soscket inteligas
                    'Me.SocketSecundario.Send(handler, "ok")
                Case "G"
                    'Ubicacion(strdatos, respuesta, 2, IDTerminal) 'procesa la informacion

                Case "C"
                    'Ubicacion(strdatos, respuesta, 4, IDTerminal) 'procesa la informacion

                Case Else
                    WinSockServer.EnviarDatos(CType(IDTerminal, System.Net.IPEndPoint), String.Format("*{0}{1}", Format(3 + 3, "00#"), "D=0"))

                    Exit Sub
            End Select

            'If respuesta = String.Empty Then
            '    'WinSockServer.EnviarDatos(CType(IDTerminal, System.Net.IPEndPoint), String.Format("*{0}{1}", Format(3 + 3, "00#"), "U=0"))
            'End If

        Catch ex As Exception
            WinSockServer.EnviarDatos(CType(IDTerminal, System.Net.IPEndPoint), String.Format("*{0}{1}", Format(3 + 3, "00#"), "U=5"))
            Exit Sub
        End Try

        Try
            
            WinSockServer.EnviarDatos(CType(IDTerminal, System.Net.IPEndPoint), respuesta)

        Catch ex As Exception
            WinSockServer.EnviarDatos(CType(IDTerminal, System.Net.IPEndPoint), String.Format("*{0}{1}", Format(3 + 3, "00#"), "U=5"))
            Exit Sub
        End Try


    End Sub

    Private Sub btniniciar_Click(sender As Object, e As EventArgs) Handles btniniciar.Click
        CheckForIllegalCrossThreadCalls = False
        If readxml() = 0 Then

            With WinSockServer
                'Establezco el puerto donde escuchar
                .PuertoDeEscucha = Me.txtport.Text

                'Comienzo la escucha
                .Escuchar()
            End With
        Else
            MsgBox("Ingrese al modulo de configuracion,para establecer parametros de inicio")
            'Regis.escribirficheroInfoinicio(String.Concat("Dashboard load ", "  |  ", "Ingrese al modulo de configuracion, para establecer parametros de inicio"))
        End If
    End Sub

    Private Sub btndatabase_Click(sender As Object, e As EventArgs) Handles btndatabase.Click
        Dim frm As New frmDatabase
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Call readxml()
        End If
    End Sub
End Class