﻿Imports LibUtil
Imports System.Data

Module ModConfigVar

    Public NameXmlDb As String = "DBDrop.xml"
    Public NameXmlABCDDB As String = "DBABDC.xml"
    Public NameXmlNUMERIC As String = "DBNUMERIC.xml"

    Public Function CreaBaseDrop() As Boolean
        Dim DbCreate As New ClsUtil

        If (DbCreate.FileExist(String.Concat(DbCreate.GetPath, "\", NameXmlDb)) = False) Then
            DbCreate.CreateDBDropDown(NameXmlDb)
            Return True
        Else
            Return False
        End If

    End Function

    Public Function BorraBaseDrop() As Boolean
        Dim DbCreate As New ClsUtil

        If (DbCreate.FileExist(String.Concat(DbCreate.GetPath, "\", NameXmlDb)) = True) Then
            DbCreate.DeleteDBDropDown(String.Concat(DbCreate.GetPath, "\", NameXmlDb))
            Return True
        Else
            Return False
        End If

    End Function

    Public Function CargarBaseDrop() As DataSet
        Dim DBDropCharging As New ClsUtil
        Dim DataDropCharging As New DataSet

        If (DBDropCharging.FileExist(String.Concat(DBDropCharging.GetPath, "\", NameXmlDb)) = True) Then
            DBDropCharging.ReadDataXml(NameXmlDb, DataDropCharging, DBDropCharging.GetPath)
        End If

        Return DataDropCharging

    End Function

    Public Function CargarBaseDropABCD() As DataSet
        Dim DBDropCharging As New ClsUtil
        Dim DataDropCharging As New DataSet
        'DataDropCharging = Nothing
        If (DBDropCharging.FileExist(String.Concat(DBDropCharging.GetPath, "\", NameXmlABCDDB)) = True) Then
            DBDropCharging.ReadDataXml(NameXmlABCDDB, DataDropCharging, DBDropCharging.GetPath)
        End If

        Return DataDropCharging

    End Function


    Public Function CargarBaseDropNumeric() As DataSet
        Dim DBDropCharging As New ClsUtil
        Dim DataDropCharging As New DataSet
        'DataDropCharging = Nothing
        If (DBDropCharging.FileExist(String.Concat(DBDropCharging.GetPath, "\", NameXmlNUMERIC)) = True) Then
            DBDropCharging.ReadDataXml(NameXmlNUMERIC, DataDropCharging, DBDropCharging.GetPath)
        End If

        Return DataDropCharging

    End Function

End Module
