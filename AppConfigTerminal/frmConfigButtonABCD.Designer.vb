﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfigButtonABCD
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DropD = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DropC = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DropB = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DropA = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DropD
        '
        Me.DropD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DropD.FormattingEnabled = True
        Me.DropD.Location = New System.Drawing.Point(20, 174)
        Me.DropD.Name = "DropD"
        Me.DropD.Size = New System.Drawing.Size(259, 21)
        Me.DropD.TabIndex = 28
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(52, 151)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 20)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Button D"
        '
        'DropC
        '
        Me.DropC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DropC.FormattingEnabled = True
        Me.DropC.Location = New System.Drawing.Point(20, 127)
        Me.DropC.Name = "DropC"
        Me.DropC.Size = New System.Drawing.Size(259, 21)
        Me.DropC.TabIndex = 26
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(53, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 20)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "Button C"
        '
        'DropB
        '
        Me.DropB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DropB.FormattingEnabled = True
        Me.DropB.Location = New System.Drawing.Point(20, 80)
        Me.DropB.Name = "DropB"
        Me.DropB.Size = New System.Drawing.Size(259, 21)
        Me.DropB.TabIndex = 24
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(53, 57)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 20)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "Button B"
        '
        'DropA
        '
        Me.DropA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DropA.FormattingEnabled = True
        Me.DropA.Location = New System.Drawing.Point(20, 30)
        Me.DropA.Name = "DropA"
        Me.DropA.Size = New System.Drawing.Size(259, 21)
        Me.DropA.TabIndex = 22
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(53, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 20)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Button A"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(693, 364)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(77, 56)
        Me.btnSave.TabIndex = 29
        Me.btnSave.Text = "Guardar"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.AppConfigTerminal.My.Resources.Resources.kp_vx520
        Me.PictureBox1.Location = New System.Drawing.Point(294, 30)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(476, 316)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 16
        Me.PictureBox1.TabStop = False
        '
        'frmConfigButtonABCD
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(795, 477)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.DropD)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DropC)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DropB)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.DropA)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "frmConfigButtonABCD"
        Me.Text = "Configurar A-B-C-D"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DropD As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DropC As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DropB As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DropA As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
End Class
