﻿Imports LibUtil

Public Class frmConfigButtonABCD

    Private Sub fillbutton()

        DropA.ValueMember = "Value"
        DropA.DisplayMember = "Text"
        DropA.DataSource = CargarBaseDrop.Tables(0)

        DropB.ValueMember = "Value"
        DropB.DisplayMember = "Text"
        DropB.DataSource = CargarBaseDrop.Tables(0)

        DropC.ValueMember = "Value"
        DropC.DisplayMember = "Text"
        DropC.DataSource = CargarBaseDrop.Tables(0)

        DropD.ValueMember = "Value"
        DropD.DisplayMember = "Text"
        DropD.DataSource = CargarBaseDrop.Tables(0)

    End Sub


    Private Sub frmConfigButtonABCD_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fillbutton()
        SelectOptionDrop()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SvButton As New ClsUtil
        Dim DsABCD As DataSet
        DsABCD = SvButton.FunctionABCD("ABCDDB.xml", DropA.SelectedValue, DropB.SelectedValue, DropC.SelectedValue, DropD.SelectedValue)
        SvButton.WriteDataXml(NameXmlABCDDB, DsABCD, SvButton.GetPath)
        SelectOptionDrop()
    End Sub

    Private Sub SelectOptionDrop()
        Dim Ds As New DataSet
        Dim ct As Integer
        Ds = CargarBaseDropABCD()

        If Ds.Tables.Count = 0 Then
            Exit Sub
        End If



        For Each row As DataRow In Ds.Tables(0).Rows
            Select Case ct
                Case 0
                    DropA.SelectedValue = row("Value")
                Case 1
                    DropB.SelectedValue = row("Value")
                Case 2
                    DropC.SelectedValue = row("Value")
                Case 3
                    DropD.SelectedValue = row("Value")
            End Select
            ct = ct + 1

        Next
    End Sub

    Private Sub DropA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropA.SelectedIndexChanged

    End Sub

    Private Sub DropB_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropB.SelectedIndexChanged

    End Sub
End Class