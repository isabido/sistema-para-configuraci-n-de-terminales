﻿Public Class frmDatabase

    Private Sub btnsave_Click(sender As System.Object, e As System.EventArgs) Handles btnsave.Click
        SaveXml()
        'Me.ShowDialog. = Windows.Forms.DialogResult.OK
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub SaveXml()
        Dim strpath As String = My.Application.Info.DirectoryPath
        Dim Obj As Object
        Dim archivo As Object
        Dim x As Integer = 0

        REM WindowsEventLog.CreateError(strpath, 20)
        Dim pathfull As String = strpath & "\config.xml"
        'If File.Exists(pathfull) Then
        '    Dim xml As New XmlDocument
        '    xml.Load(pathfull)
        '    Dim Element As XmlElement = xml.CreateElement("cadena")
        '    Dim strNewvideo As String = "<Cad bomba=""" & B & """>" & _
        '                               "<subnodo1>" & cadenasave & "</subnodo1> </Cad>"
        '    Element.InnerXml = strNewvideo
        '    xml.DocumentElement.AppendChild(Element)
        '    xml.Save(pathfull)
        'Else
        Dim auto As Integer
        If Me.chkinicioautomatico.Checked = True Then
            auto = 1
        Else
            auto = 0
        End If
        Obj = CreateObject("Scripting.FileSystemobject")
        archivo = Obj.createtextfile(pathfull, True)
        archivo.writeline("<?xml version=""1.0"" encoding='ISO-8859-1' standalone=""no"" ?>")
        archivo.writeline("<config>")
        archivo.writeline("<ID id=""1"">")
        archivo.writeline("<Puerto>" & Me.txtportes.Text & "</Puerto>")
        archivo.writeline("<automatico>" & auto & "</automatico>")
        archivo.writeline("<Servidor>" & Me.txtservidor.Text & "</Servidor>")
        archivo.writeline("<BaseDatos>" & Me.txtbasedatos.Text & "</BaseDatos>")
        archivo.writeline("<Usuario>" & Me.txtusuario.Text & "</Usuario>")
        archivo.writeline("<Contraseña>" & Me.txtcontraseña.Text & "</Contraseña>")
        archivo.writeline("<PuertoDb>" & Me.txtpuerto.Text & "</PuertoDb>")
        archivo.writeline("</ID>")
        archivo.writeline("</config>")
        archivo.close()
    End Sub


    Private Function readxml() As Integer
        Dim dataTable As DataTable
        Dim dataset As New DataSet
        Dim dt As New DataSet
        Dim strpath As String = My.Application.Info.DirectoryPath

        Try
            Dim pathfull As String = strpath & "\config.xml"
            dataset.Clear()

            For Each dataTable In dataset.Tables
                dataTable.BeginLoadData()
            Next

            dataset.ReadXml(pathfull)

            For Each dataTable In dataset.Tables
                dataTable.EndLoadData()
            Next
            'dataset.Tables(0).Select(String.Format("bomba={0}", B), "bomba ASC")

            For Each rd As DataRow In dataset.Tables(0).Rows
                Me.txtportes.Text = rd("Puerto")
                Me.txtservidor.Text = rd("Servidor")
                Me.txtbasedatos.Text = rd("BaseDatos")
                Me.txtusuario.Text = rd("Usuario")
                Me.txtcontraseña.Text = rd("Contraseña")
                Me.txtpuerto.Text = rd("PuertoDb")
                If rd("automatico") = 1 Then
                    Me.chkinicioautomatico.Checked = True
                Else
                    Me.chkinicioautomatico.Checked = False
                End If

            Next
            Return 0
        Catch ex As Exception
            Return 1
        End Try
        'WindowsEventLog.CreateError(strpath, 20)

    End Function

    Private Sub frmDatabase_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        readxml()
    End Sub
End Class