﻿using LibUtil.Models;
using LibUtil.utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppConfigTerminal2
{
    public partial class AddGroupForm : Form
    {
        int _id = -1;

        public AddGroupForm()
        {
            InitializeComponent();
        }

        public AddGroupForm(int id)
        {
            InitializeComponent();
            this._id = id;
            if(this._id != -1)
                this.getGroup();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public void getGroup()
        {
            Group _group = new Group();
            _group.Id = _id;
            if(_id != -1)
               _group = _group.getGroup(_id);
            if (_group != null)
                txtGroupName.Text = _group.Name;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            switch (_id)
            {
                case -1:
                    this.createGroup();
                        break;
                default:
                    this.editGroup();
                    break;

                    

            }
            
            //DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        public void editGroup()
        {
            Group group = new Group();
            group.Id = _id;
            group.Name = txtGroupName.Text.Trim();
            if(!Utils.isEmpty(group.Name) && group.Id != -1)
            {
                bool band = group.updateGroup();
                DialogResult = System.Windows.Forms.DialogResult.OK;
                MessageBox.Show("Se han realizado las modificaciones");
                //this.Close();
                txtGroupName.Text = String.Empty;
            }
            else
            {
                MessageBox.Show(Messages.ErrorSaving);
            }
            
        }

        public void createGroup()
        {
            Group group = new Group();
            group.Name = txtGroupName.Text.Trim();
            if (!Utils.isEmpty(group.Name))
            {
                long band = group.insertGroup();
                if(band != 0)
                {
                    MessageBox.Show("Grupo creado!!!");
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    //this.Close();
                    txtGroupName.Text = String.Empty;
                }
                else
                {
                    MessageBox.Show("No se ha podido crear el grupo.");
                }
                    
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
