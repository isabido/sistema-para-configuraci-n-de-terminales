﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibUtil.utils;
using LibUtil.Models;

namespace AppConfigTerminal2
{
    public partial class ContactForm : Form
    {
        private int stationId = -1;
        private int contactId = -1;
        private string email;
        private string name;
        private string phone;
        private string stationNumber;

        public ContactForm()
        {
            InitializeComponent();
            this.stationId = 4;
            this.contactId = 5;
            fillForm();
        }

        public ContactForm(int id, string stationNumber)
        {
            InitializeComponent();
            this.stationId = id;
            this.stationNumber = stationNumber;
        }

        public ContactForm(int stationId, string stationNumber, int contactId)
        {
            InitializeComponent();
            this.stationId = stationId;
            this.contactId = contactId;
            this.stationNumber = stationNumber;
            this.fillForm();
        }

        public void fillForm()
        {
            Contact contact = new Contact();
            contact = contact.getContact(contactId);
            if(contact != null)
            {
                this.txtContactName.Text = contact.Name;
                this.txtEmail.Text = contact.Email;
                this.txtPhoneContact.Text = contact.Phone;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.email = this.txtEmail.Text.Trim();
            this.phone = this.txtPhoneContact.Text.Trim();
            this.name = this.txtContactName.Text.Trim();

            if (this.stationId != -1 && this.contactId == -1)
            {
                this.insertContact();
            }
            else if(this.stationId != -1 && this.contactId != -1)
            {
                this.update();
            }
            else
            {
                MessageBox.Show("Ha habido un problema, cierre la ventana e intente de nuevo.");
            }
            
        }

        public void insertContact()
        {
            
            if (!Utils.isEmpty(email) && !Utils.isEmail(email))
            {
                MessageBox.Show("El formato de email es incorrecto");
                return;
            }

                if (!Utils.isEmpty(name) && !Utils.isEmpty(phone))
                {
                    Contact contact = new Contact();
                    contact.StationId = this.stationId;
                    contact.Name = this.name;
                    contact.Email = this.email;
                    contact.Phone = this.phone;
                    if (contact.insertContact())
                    {
                        MessageBox.Show("Se ha almacenado el contacto");
                    }
                }
                else
                {
                    MessageBox.Show(Messages.EmptyString);
                }
            
        }
        public void update()
        {
            if (!Utils.isEmpty(email) && !Utils.isEmail(email))
            {
                MessageBox.Show("El formato de email es incorrecto");
                return;
            }

            if (!Utils.isEmpty(name) && !Utils.isEmpty(phone))
            {
                Contact contact = new Contact();
                contact.Id = contactId;
                contact.StationId = this.stationId;
                contact.Name = this.name;
                contact.Email = this.email;
                contact.Phone = this.phone;
                if (contact.updateContact())
                {
                    MessageBox.Show("Se ha actualizado el contacto");
                }
            }
            else
            {
                MessageBox.Show(Messages.EmptyString);
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
    }
}
