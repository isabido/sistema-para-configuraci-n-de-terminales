﻿using ServiceStack.OrmLite;
using System;
using System.Windows.Forms;
using LibUtil;
using LibUtil.utils;
using LibUtil.Models;
using NLog;
using AppConfigTerminal;

namespace AppConfigTerminal2
{
    public partial class Form1 : Form
    {
        string user;
        string password;

        String connectionString = "Data Source=\\App_Data\\mydb.db;Version=3;";
        public Form1()
        {
            InitializeComponent();
            Init_DB();
        }

        public void Init_DB()
        {
            try
            {
                var dbFactory = new OrmLiteConnectionFactory(connectionString, SqliteDialect.Provider);
            }
            catch(Exception e)
            {
                Console.Out.WriteLine(e);
            }
            
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            user = txtUser.Text.Trim();
            password = txtPassword.Text.Trim();
            if(!Utils.isEmpty(user) && !Utils.isEmpty(password))
            {
                try
                {
                    User u = new User();
                    u.Nombre = user;
                    u.Password = password;
                    u = u.login();
                    if(u != null)
                    {
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                        //AppConfigTerminal.Form1 f = new AppConfigTerminal.Form1();
                        //this.Hide();
                        //f.Show();
                        //this.Close();
                    }
                    else
                    {
                        DialogResult = System.Windows.Forms.DialogResult.None;
                    }
                    
                }
                catch(Exception ex)
                {
                    DbConnection.logger.Error(ex);
                   
                }
               
            }
        }
    }
}
