﻿using LibUtil.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsApplication3;

namespace AppConfigTerminal2
{
    public partial class GPForm : Form
    {
        bool band = false;
        List<Group> gr = null;
        GroupForm _group;
        public GPForm()
        {
            InitializeComponent();
            // fill();
            // This line of code is generated by Data Source Configuration Wizard
            //Group g = new Group();
            //var gG = g.getGroups();
            //gridControl1.DataSource = gG;
            if (!band)
            {
                btnCreate.Enabled = true;
                btnDelete.Enabled = false;
                btnView.Enabled = false;
                btnEdit.Enabled = false;
            }
            fill();
            
        }

        public void fill()
        {
            Group g = new Group();
            gr = g.getGroups3();

            gridControl1.DataSource = new NestedRecords(gr);
            init();
            //gridControl1.Columns["Id"].Width = 50;
            //MessageBox.Show(this.dgvGroups.Rows[0].Cells["Name"].Value.ToString());
            //this.dgvGroups.Rows.RemoveAt(0);
            //foreach (var i in gG.ToList<Group>())
            //{
            //    MessageBox.Show(i.Name);
            //}

        }

        public void init()
        {
            
        }

        private void GPForm_Load(object sender, EventArgs e)
        {

        }

        private void prueba()
        {
            //this.rowIndex = this.gridControl1.CurrentRow.Index;
            int rowHandle = gridView1.FocusedRowHandle;

            var d = (Group)gridView1.GetRow(0);
            var x = gridView1.GetRowCellDisplayText(rowHandle, gridView1.FocusedColumn).ToString();
            var y = gridView1.GetRowCellDisplayText(rowHandle, gridView1.Columns["Name"]).ToString();
            //if(gridView1.IsGroupRow(rowHandle))
            MessageBox.Show(y);

        }

        private void gridView1_FocusedColumnChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs e)
        {
            prueba();
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (gridView1.GetRowExpanded(gridView1.FocusedRowHandle))
                SelectChildRows();
        }

        private void gridView1_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            prueba();
        }


        private void SelectChildRows()
        {
            int handle = gridView1.FocusedRowHandle;
            //MessageBox.Show(gridView1.GroupCount.ToString());
            var m = gridView1.IsGroupRow(handle);
            //MessageBox.Show(m.ToString());
            if (gridView1.GroupCount > 0)
            {
                gridView1.BeginSelection();
                gridView1.ClearSelection();
                
                if (!gridView1.IsDataRow(handle))
                {
                    gridView1.SelectRow(handle);
                    int start = gridView1.GetChildRowHandle(handle, 0);
                    int end = gridView1.GetChildRowHandle(handle, gridView1.GetChildRowCount(handle) - 1);
                    gridView1.SelectRows(start, end);
                }
                gridView1.EndSelection();
            }
        }

        private void gridView1_GroupRowExpanded(object sender, DevExpress.XtraGrid.Views.Base.RowEventArgs e)
        {
            SelectChildRows();
        }

        private void gridView1_FocusedRowChanged_1(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int i = e.FocusedRowHandle;
            var d = gridView1.GetRow(i);
            //MessageBox.Show(i.ToString());
            var b = gr.ElementAt(i);
            this.enableButtons();
            if (gridView1.GetRowExpanded(gridView1.FocusedRowHandle))
                SelectChildRows();
            //MessageBox.Show("HOLA");
        }

        

        private void gridView1_MasterRowExpanded(object sender, DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventArgs e)
        {
                SelectChildRows();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void gridControl1_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        public void enableButtons()
        {
            btnCreate.Enabled = true;
            btnDelete.Enabled = true;
            btnView.Enabled = true;
            btnEdit.Enabled = true;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            AddGroupForm groupF = new AddGroupForm();
            if (groupF.ShowDialog() == DialogResult.OK)
                this.fill();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
             try
            {
                var id = gridView1.FocusedRowHandle;
                //int id = this.rowIndex = (int)this.dgvGroups.CurrentRow.Cells["Id"].Value;
                var b = gr.ElementAt(id);
                AddGroupForm _group = new AddGroupForm(b.Id);
                if (_group.ShowDialog() == DialogResult.OK)
                    this.fill();
            }
            catch(Exception ex)
            {

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var id = gridView1.FocusedRowHandle;
                //int id = this.rowIndex = (int)this.dgvGroups.CurrentRow.Cells["Id"].Value;
                var b = gr.ElementAt(id);
                DialogResult result1 = MessageBox.Show("¿Desea eliminar al grupo "+b.Name+"?", "Eliminar Grupo", MessageBoxButtons.YesNo);
                if( result1 == DialogResult.Yes)
                {
                    this.deleteRow(b.Id, id);
                }
                
            }
            catch (Exception ex)
            {

            }
        }

        public void deleteRow(int id, int idRow)
        {
            Group g = new Group();
            if (g.deleteGroup(id))
            {
                gr.RemoveAt(idRow);
                gridView1.DeleteRow(idRow);
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                var id = gridView1.FocusedRowHandle;
                //int id = this.rowIndex = (int)this.dgvGroups.CurrentRow.Cells["Id"].Value;
                var b = gr.ElementAt(id);
                _group = new GroupForm(gr.ElementAt(id));
                _group.MdiParent = this.ParentForm;
                _group.Show();
           
                //if (_group.ShowDialog() == DialogResult.OK)
                //    this.fill();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
