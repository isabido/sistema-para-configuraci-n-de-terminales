﻿using LibUtil.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppConfigTerminal2
{
    public partial class GroupForm : Form
    {
        Group group = null;
        BindingList<Station> list_station = null;
        public GroupForm()
        {
            InitializeComponent();
            fillGrid();
        }

        public GroupForm(Group group)
        {
            InitializeComponent();
            this.group = group;
            this.lblGroupName.Text = group.Name;
            fillGrid();
            this.Text = "Grupo: " + group.Name;
        }

        public void fillGrid()
        {
            Station sp = new Station();
            list_station = new BindingList<Station>(sp.getStations(group.Id));
            gridControl1.DataSource = list_station;
            
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.fillGrid();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                var id = gridView1.FocusedRowHandle;
                //int id = this.rowIndex = (int)this.dgvGroups.CurrentRow.Cells["Id"].Value;
                var st = list_station.ElementAt(id);
                StationForm _group = new StationForm(group, st.Id);
                _group.MdiParent = this.ParentForm;
                _group.Show();

                //if (_group.ShowDialog() == DialogResult.OK)
                //    this.fill();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Station st = new Station();
                st.Id = 0;
                StationForm _group = new StationForm(group, st.Id);
                _group.MdiParent = this.ParentForm;
                _group.Show();

                //if (_group.ShowDialog() == DialogResult.OK)
                //    this.fill();
            }
            catch (Exception ex)
            {

            }

        }
    }
}
