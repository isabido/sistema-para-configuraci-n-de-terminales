﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibUtil.Models;

namespace AppConfigTerminal2
{
    public partial class GroupsForm : Form
    {
        int rowIndex = -1;
        
        public GroupsForm()
        {
            InitializeComponent();
        }

        public void fill()
        {
            Group g = new Group();
            var gG = g.getGroups();
            dgvGroups.DataSource = gG;
            this.dgvGroups.Columns["Id"].Width = 50;
            //MessageBox.Show(this.dgvGroups.Rows[0].Cells["Name"].Value.ToString());
            //this.dgvGroups.Rows.RemoveAt(0);
            //foreach (var i in gG.ToList<Group>())
            //{
            //    MessageBox.Show(i.Name);
            //}

        }


        private void GroupsForm_Load(object sender, EventArgs e)
        {
            this.fill();
        }

        private void dgvGroups_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                this.rowIndex = this.dgvGroups.CurrentRow.Index;
            }catch(Exception ex)
            {
                MessageBox.Show("No existen grupos!");
            }
            
           
        }

        private void dgvGroups_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.dgvGroups.Rows[e.RowIndex].Selected = true;
                this.rowIndex = e.RowIndex;
                this.dgvGroups.CurrentCell = this.dgvGroups.Rows[e.RowIndex].Cells[1];
                this.contextMenuStrip1.Show(this.dgvGroups, e.Location);
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void contextMenuStrip1_Click(object sender, EventArgs e)
        {
            if (!this.dgvGroups.Rows[this.rowIndex].IsNewRow)
            {
               
                int id = (int)this.dgvGroups.Rows[rowIndex].Cells["Id"].Value;
                this.deleteRow(id);                
            }
        }

        public void deleteRow(int id)
        {
            Group g = new Group();
            if (g.deleteGroup(id))
            {
                this.dgvGroups.Rows.RemoveAt(this.rowIndex);
                this.rowIndex = -1;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int id = (int)this.dgvGroups.Rows[rowIndex].Cells["Id"].Value;
                this.deleteRow(id);
            }
            catch(Exception ex)
            {

            }
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddGroupForm groupF = new AddGroupForm();
            if (groupF.ShowDialog() == DialogResult.OK)
                this.fill();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                int id = this.rowIndex = (int)this.dgvGroups.CurrentRow.Cells["Id"].Value;
                AddGroupForm _group = new AddGroupForm(id);
                if (_group.ShowDialog() == DialogResult.OK)
                    this.fill();
            }
            catch(Exception ex)
            {

            }
            
        }
    }
}
