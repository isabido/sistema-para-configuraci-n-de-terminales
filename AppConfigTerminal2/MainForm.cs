﻿using AppConfigTerminal;
using LibUtil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppConfigTerminal2
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            GPForm gp_form = new GPForm();
            gp_form.MdiParent = this;
            gp_form.Show();
        }

        private void numericoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void crearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DbConnection db = new DbConnection();
            if (db.Init_DB())
            {
                MessageBox.Show("La base de datos ha sido creada");
            }
            else
            {
                MessageBox.Show("No se ha podido crear la base de datos");
            }
        }

        private void gruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //GroupsForm gd = new GroupsForm();
            //gd.MdiParent = this;
            //gd.Show();
            GPForm g = new GPForm();
            g.MdiParent = this;
            g.Show();
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserListForm l_u = new UserListForm();
            l_u.MdiParent = this;
            l_u.Show();
        }

        private void regenerarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DbConnection db = new DbConnection();
            if (db.Init_DB())
            {
                MessageBox.Show("La base de datos ha sido reiniciado");
            }
            else
            {
                MessageBox.Show("No se ha podido reiniciar la base de datos");
            }
        }
    }
}
