﻿using LibUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppConfigTerminal2
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new MainForm());
            DbConnection db = new DbConnection();
            if (db.First_DB())
            {
                Form1 us = new Form1();
                if (us.ShowDialog() != DialogResult.OK) return;
                Application.Run(new MainForm());
            }
            else
            {
                MessageBox.Show("No se puede iniciar el programa");
            }
            
        }
    }
}
