﻿namespace AppConfigTerminal2
{
    partial class StationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtEstation = new System.Windows.Forms.TextBox();
            this.btnSaveStation = new System.Windows.Forms.Button();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tpTerminals = new DevExpress.XtraTab.XtraTabPage();
            this.gcTerminals = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnAddTerminal = new System.Windows.Forms.Button();
            this.txtPos = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNumSerie = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tpContacts = new DevExpress.XtraTab.XtraTabPage();
            this.btnAddContacts = new System.Windows.Forms.Button();
            this.gcContacts = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtPhone = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtContactName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tpServers = new DevExpress.XtraTab.XtraTabPage();
            this.btnSaveServers = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtLealtadPort = new System.Windows.Forms.MaskedTextBox();
            this.txtLealtadIP = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtGPRSIP = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtGPRSPort = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtMonederoIP = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMonederoPort = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtEfectivosPort = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEfectivosIP = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtValesIP = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtValesPort = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnEditStation = new System.Windows.Forms.Button();
            this.btnDeleteStation = new System.Windows.Forms.Button();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.btnTerminal = new System.Windows.Forms.Button();
            this.btnTicket = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tpTerminals.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTerminals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.tpContacts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.tpServers.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(37, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Número de Estación:";
            // 
            // txtEstation
            // 
            this.txtEstation.Enabled = false;
            this.txtEstation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstation.Location = new System.Drawing.Point(200, 87);
            this.txtEstation.Name = "txtEstation";
            this.txtEstation.Size = new System.Drawing.Size(125, 26);
            this.txtEstation.TabIndex = 7;
            this.txtEstation.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEstation_KeyUp);
            // 
            // btnSaveStation
            // 
            this.btnSaveStation.BackgroundImage = global::AppConfigTerminal2.Properties.Resources._1450392123_save;
            this.btnSaveStation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSaveStation.Location = new System.Drawing.Point(35, 12);
            this.btnSaveStation.Name = "btnSaveStation";
            this.btnSaveStation.Size = new System.Drawing.Size(49, 49);
            this.btnSaveStation.TabIndex = 9;
            this.btnSaveStation.UseVisualStyleBackColor = true;
            this.btnSaveStation.Click += new System.EventHandler(this.button4_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(35, 133);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tpTerminals;
            this.xtraTabControl1.Size = new System.Drawing.Size(689, 498);
            this.xtraTabControl1.TabIndex = 10;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpTerminals,
            this.tpContacts,
            this.tpServers});
            // 
            // tpTerminals
            // 
            this.tpTerminals.Controls.Add(this.gcTerminals);
            this.tpTerminals.Controls.Add(this.btnAddTerminal);
            this.tpTerminals.Controls.Add(this.txtPos);
            this.tpTerminals.Controls.Add(this.label4);
            this.tpTerminals.Controls.Add(this.txtNumSerie);
            this.tpTerminals.Controls.Add(this.label3);
            this.tpTerminals.Controls.Add(this.txtModel);
            this.tpTerminals.Controls.Add(this.label2);
            this.tpTerminals.Image = global::AppConfigTerminal2.Properties.Resources.terminal;
            this.tpTerminals.Name = "tpTerminals";
            this.tpTerminals.PageEnabled = false;
            this.tpTerminals.Size = new System.Drawing.Size(683, 458);
            this.tpTerminals.Text = "Terminales";
            // 
            // gcTerminals
            // 
            this.gcTerminals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcTerminals.Location = new System.Drawing.Point(29, 129);
            this.gcTerminals.MainView = this.gridView1;
            this.gcTerminals.Name = "gcTerminals";
            this.gcTerminals.Size = new System.Drawing.Size(629, 284);
            this.gcTerminals.TabIndex = 7;
            this.gcTerminals.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcTerminals;
            this.gridView1.Name = "gridView1";
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // btnAddTerminal
            // 
            this.btnAddTerminal.BackgroundImage = global::AppConfigTerminal2.Properties.Resources._1450392945_plus_sign;
            this.btnAddTerminal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddTerminal.Enabled = false;
            this.btnAddTerminal.Location = new System.Drawing.Point(606, 71);
            this.btnAddTerminal.Name = "btnAddTerminal";
            this.btnAddTerminal.Size = new System.Drawing.Size(32, 32);
            this.btnAddTerminal.TabIndex = 6;
            this.btnAddTerminal.UseVisualStyleBackColor = true;
            this.btnAddTerminal.Click += new System.EventHandler(this.btnAddTerminal_Click);
            // 
            // txtPos
            // 
            this.txtPos.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtPos.Location = new System.Drawing.Point(461, 43);
            this.txtPos.Name = "txtPos";
            this.txtPos.Size = new System.Drawing.Size(174, 22);
            this.txtPos.TabIndex = 5;
            this.txtPos.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPos_KeyUp);
            this.txtPos.Leave += new System.EventHandler(this.txtPos_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label4.Location = new System.Drawing.Point(458, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 14);
            this.label4.TabIndex = 4;
            this.label4.Text = "POS";
            // 
            // txtNumSerie
            // 
            this.txtNumSerie.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtNumSerie.Location = new System.Drawing.Point(242, 43);
            this.txtNumSerie.Name = "txtNumSerie";
            this.txtNumSerie.Size = new System.Drawing.Size(174, 22);
            this.txtNumSerie.TabIndex = 3;
            this.txtNumSerie.Leave += new System.EventHandler(this.txtNumSerie_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label3.Location = new System.Drawing.Point(239, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "Número de Serie";
            // 
            // txtModel
            // 
            this.txtModel.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtModel.Location = new System.Drawing.Point(29, 43);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(174, 22);
            this.txtModel.TabIndex = 1;
            this.txtModel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtModel_KeyUp);
            this.txtModel.Leave += new System.EventHandler(this.txtModel_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label2.Location = new System.Drawing.Point(26, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Modelo";
            // 
            // tpContacts
            // 
            this.tpContacts.Controls.Add(this.btnAddContacts);
            this.tpContacts.Controls.Add(this.gcContacts);
            this.tpContacts.Controls.Add(this.txtPhone);
            this.tpContacts.Controls.Add(this.label7);
            this.tpContacts.Controls.Add(this.txtEmail);
            this.tpContacts.Controls.Add(this.label6);
            this.tpContacts.Controls.Add(this.txtContactName);
            this.tpContacts.Controls.Add(this.label5);
            this.tpContacts.Image = global::AppConfigTerminal2.Properties.Resources.phone_book;
            this.tpContacts.Name = "tpContacts";
            this.tpContacts.PageEnabled = false;
            this.tpContacts.Size = new System.Drawing.Size(683, 458);
            this.tpContacts.Text = "Contactos";
            // 
            // btnAddContacts
            // 
            this.btnAddContacts.BackgroundImage = global::AppConfigTerminal2.Properties.Resources._1450392945_plus_sign;
            this.btnAddContacts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddContacts.Enabled = false;
            this.btnAddContacts.Location = new System.Drawing.Point(617, 74);
            this.btnAddContacts.Name = "btnAddContacts";
            this.btnAddContacts.Size = new System.Drawing.Size(32, 32);
            this.btnAddContacts.TabIndex = 11;
            this.btnAddContacts.UseVisualStyleBackColor = true;
            this.btnAddContacts.Click += new System.EventHandler(this.btnAddContacts_Click);
            // 
            // gcContacts
            // 
            this.gcContacts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcContacts.Location = new System.Drawing.Point(25, 129);
            this.gcContacts.MainView = this.gridView2;
            this.gcContacts.Name = "gcContacts";
            this.gcContacts.Size = new System.Drawing.Size(631, 274);
            this.gcContacts.TabIndex = 9;
            this.gcContacts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gcContacts.DoubleClick += new System.EventHandler(this.gcContacts_DoubleClick);
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcContacts;
            this.gridView2.Name = "gridView2";
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            // 
            // txtPhone
            // 
            this.txtPhone.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtPhone.Location = new System.Drawing.Point(557, 46);
            this.txtPhone.Mask = "000-000-0000";
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(99, 22);
            this.txtPhone.TabIndex = 8;
            this.txtPhone.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPhone_KeyUp);
            this.txtPhone.Leave += new System.EventHandler(this.txtPhone_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label7.Location = new System.Drawing.Point(554, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 14);
            this.label7.TabIndex = 6;
            this.label7.Text = "Teléfono";
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtEmail.Location = new System.Drawing.Point(309, 46);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(222, 22);
            this.txtEmail.TabIndex = 5;
            this.txtEmail.Leave += new System.EventHandler(this.txtEmail_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label6.Location = new System.Drawing.Point(306, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 14);
            this.label6.TabIndex = 4;
            this.label6.Text = "Email";
            // 
            // txtContactName
            // 
            this.txtContactName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtContactName.Location = new System.Drawing.Point(25, 46);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(267, 22);
            this.txtContactName.TabIndex = 3;
            this.txtContactName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtContactName_KeyUp);
            this.txtContactName.Leave += new System.EventHandler(this.txtContactName_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label5.Location = new System.Drawing.Point(22, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 14);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nombre";
            // 
            // tpServers
            // 
            this.tpServers.Controls.Add(this.btnSaveServers);
            this.tpServers.Controls.Add(this.groupBox5);
            this.tpServers.Controls.Add(this.groupBox4);
            this.tpServers.Controls.Add(this.groupBox3);
            this.tpServers.Controls.Add(this.groupBox2);
            this.tpServers.Controls.Add(this.groupBox1);
            this.tpServers.Image = global::AppConfigTerminal2.Properties.Resources._1450502244_router;
            this.tpServers.Name = "tpServers";
            this.tpServers.Size = new System.Drawing.Size(683, 458);
            this.tpServers.Text = "Servidores";
            // 
            // btnSaveServers
            // 
            this.btnSaveServers.BackgroundImage = global::AppConfigTerminal2.Properties.Resources._1450392123_save;
            this.btnSaveServers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSaveServers.Location = new System.Drawing.Point(616, 18);
            this.btnSaveServers.Name = "btnSaveServers";
            this.btnSaveServers.Size = new System.Drawing.Size(49, 49);
            this.btnSaveServers.TabIndex = 13;
            this.btnSaveServers.UseVisualStyleBackColor = true;
            this.btnSaveServers.Click += new System.EventHandler(this.btnSaveServers_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtLealtadPort);
            this.groupBox5.Controls.Add(this.txtLealtadIP);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Location = new System.Drawing.Point(22, 345);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(643, 62);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Lealtad";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label15.Location = new System.Drawing.Point(134, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 14);
            this.label15.TabIndex = 18;
            this.label15.Text = "IP";
            // 
            // txtLealtadPort
            // 
            this.txtLealtadPort.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtLealtadPort.Location = new System.Drawing.Point(380, 20);
            this.txtLealtadPort.Mask = "99999";
            this.txtLealtadPort.Name = "txtLealtadPort";
            this.txtLealtadPort.Size = new System.Drawing.Size(104, 22);
            this.txtLealtadPort.TabIndex = 21;
            this.txtLealtadPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLealtadIP
            // 
            this.txtLealtadIP.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtLealtadIP.Location = new System.Drawing.Point(157, 20);
            this.txtLealtadIP.Mask = "###.###.###.###";
            this.txtLealtadIP.Name = "txtLealtadIP";
            this.txtLealtadIP.Size = new System.Drawing.Size(167, 22);
            this.txtLealtadIP.TabIndex = 19;
            this.txtLealtadIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label14.Location = new System.Drawing.Point(330, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 14);
            this.label14.TabIndex = 20;
            this.label14.Text = "Puerto";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtGPRSIP);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.txtGPRSPort);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Location = new System.Drawing.Point(22, 277);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(643, 62);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "GPRS";
            // 
            // txtGPRSIP
            // 
            this.txtGPRSIP.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtGPRSIP.Location = new System.Drawing.Point(157, 20);
            this.txtGPRSIP.Mask = "###.###.###.###";
            this.txtGPRSIP.Name = "txtGPRSIP";
            this.txtGPRSIP.Size = new System.Drawing.Size(167, 22);
            this.txtGPRSIP.TabIndex = 13;
            this.txtGPRSIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label13.Location = new System.Drawing.Point(134, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 14);
            this.label13.TabIndex = 12;
            this.label13.Text = "IP";
            // 
            // txtGPRSPort
            // 
            this.txtGPRSPort.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtGPRSPort.Location = new System.Drawing.Point(380, 20);
            this.txtGPRSPort.Mask = "99999";
            this.txtGPRSPort.Name = "txtGPRSPort";
            this.txtGPRSPort.Size = new System.Drawing.Size(104, 22);
            this.txtGPRSPort.TabIndex = 15;
            this.txtGPRSPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label12.Location = new System.Drawing.Point(330, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 14);
            this.label12.TabIndex = 14;
            this.label12.Text = "Puerto";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtMonederoIP);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtMonederoPort);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(22, 209);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(643, 62);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Monedero";
            // 
            // txtMonederoIP
            // 
            this.txtMonederoIP.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtMonederoIP.Location = new System.Drawing.Point(157, 20);
            this.txtMonederoIP.Mask = "###.###.###.###";
            this.txtMonederoIP.Name = "txtMonederoIP";
            this.txtMonederoIP.Size = new System.Drawing.Size(167, 22);
            this.txtMonederoIP.TabIndex = 7;
            this.txtMonederoIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label11.Location = new System.Drawing.Point(134, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 14);
            this.label11.TabIndex = 6;
            this.label11.Text = "IP";
            // 
            // txtMonederoPort
            // 
            this.txtMonederoPort.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtMonederoPort.Location = new System.Drawing.Point(380, 20);
            this.txtMonederoPort.Mask = "99999";
            this.txtMonederoPort.Name = "txtMonederoPort";
            this.txtMonederoPort.Size = new System.Drawing.Size(104, 22);
            this.txtMonederoPort.TabIndex = 9;
            this.txtMonederoPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label10.Location = new System.Drawing.Point(330, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 14);
            this.label10.TabIndex = 8;
            this.label10.Text = "Puerto";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtEfectivosPort);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtEfectivosIP);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(22, 141);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(643, 62);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Efectivos";
            // 
            // txtEfectivosPort
            // 
            this.txtEfectivosPort.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtEfectivosPort.Location = new System.Drawing.Point(380, 20);
            this.txtEfectivosPort.Mask = "99999";
            this.txtEfectivosPort.Name = "txtEfectivosPort";
            this.txtEfectivosPort.Size = new System.Drawing.Size(104, 22);
            this.txtEfectivosPort.TabIndex = 3;
            this.txtEfectivosPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label9.Location = new System.Drawing.Point(330, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 14);
            this.label9.TabIndex = 2;
            this.label9.Text = "Puerto";
            // 
            // txtEfectivosIP
            // 
            this.txtEfectivosIP.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtEfectivosIP.Location = new System.Drawing.Point(157, 20);
            this.txtEfectivosIP.Mask = "###.###.###.###";
            this.txtEfectivosIP.Name = "txtEfectivosIP";
            this.txtEfectivosIP.Size = new System.Drawing.Size(167, 22);
            this.txtEfectivosIP.TabIndex = 1;
            this.txtEfectivosIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label8.Location = new System.Drawing.Point(134, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "IP";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtValesIP);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtValesPort);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Location = new System.Drawing.Point(22, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(643, 62);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vales";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtValesIP
            // 
            this.txtValesIP.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtValesIP.Location = new System.Drawing.Point(157, 20);
            this.txtValesIP.Mask = "###.###.###.###";
            this.txtValesIP.Name = "txtValesIP";
            this.txtValesIP.Size = new System.Drawing.Size(167, 22);
            this.txtValesIP.TabIndex = 7;
            this.txtValesIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label17.Location = new System.Drawing.Point(134, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(18, 14);
            this.label17.TabIndex = 6;
            this.label17.Text = "IP";
            // 
            // txtValesPort
            // 
            this.txtValesPort.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txtValesPort.Location = new System.Drawing.Point(380, 20);
            this.txtValesPort.Mask = "99999";
            this.txtValesPort.Name = "txtValesPort";
            this.txtValesPort.Size = new System.Drawing.Size(104, 22);
            this.txtValesPort.TabIndex = 9;
            this.txtValesPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label16.Location = new System.Drawing.Point(330, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 14);
            this.label16.TabIndex = 8;
            this.label16.Text = "Puerto";
            // 
            // btnEditStation
            // 
            this.btnEditStation.BackgroundImage = global::AppConfigTerminal2.Properties.Resources._1450392126_edit;
            this.btnEditStation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEditStation.Location = new System.Drawing.Point(255, 13);
            this.btnEditStation.Name = "btnEditStation";
            this.btnEditStation.Size = new System.Drawing.Size(49, 49);
            this.btnEditStation.TabIndex = 11;
            this.btnEditStation.UseVisualStyleBackColor = true;
            this.btnEditStation.Click += new System.EventHandler(this.btnEditStation_Click);
            // 
            // btnDeleteStation
            // 
            this.btnDeleteStation.BackgroundImage = global::AppConfigTerminal2.Properties.Resources._1450393179_trash;
            this.btnDeleteStation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDeleteStation.Enabled = false;
            this.btnDeleteStation.Location = new System.Drawing.Point(310, 13);
            this.btnDeleteStation.Name = "btnDeleteStation";
            this.btnDeleteStation.Size = new System.Drawing.Size(49, 50);
            this.btnDeleteStation.TabIndex = 12;
            this.btnDeleteStation.UseVisualStyleBackColor = true;
            this.btnDeleteStation.Click += new System.EventHandler(this.btnDeleteStation_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.BackgroundImage = global::AppConfigTerminal2.Properties.Resources._1450392945_plus_sign;
            this.btnAddNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddNew.Location = new System.Drawing.Point(90, 12);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(49, 49);
            this.btnAddNew.TabIndex = 13;
            this.btnAddNew.UseVisualStyleBackColor = true;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnTerminal
            // 
            this.btnTerminal.BackgroundImage = global::AppConfigTerminal2.Properties.Resources.terminal_3;
            this.btnTerminal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTerminal.Location = new System.Drawing.Point(145, 12);
            this.btnTerminal.Name = "btnTerminal";
            this.btnTerminal.Size = new System.Drawing.Size(49, 49);
            this.btnTerminal.TabIndex = 14;
            this.btnTerminal.UseVisualStyleBackColor = true;
            this.btnTerminal.Click += new System.EventHandler(this.btnTerminal_Click);
            // 
            // btnTicket
            // 
            this.btnTicket.BackgroundImage = global::AppConfigTerminal2.Properties.Resources.ticket_1;
            this.btnTicket.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTicket.Enabled = false;
            this.btnTicket.Location = new System.Drawing.Point(200, 13);
            this.btnTicket.Name = "btnTicket";
            this.btnTicket.Size = new System.Drawing.Size(49, 49);
            this.btnTicket.TabIndex = 15;
            this.btnTicket.UseVisualStyleBackColor = true;
            this.btnTicket.Click += new System.EventHandler(this.btnTicket_Click);
            // 
            // StationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 642);
            this.Controls.Add(this.btnTicket);
            this.Controls.Add(this.btnTerminal);
            this.Controls.Add(this.btnAddNew);
            this.Controls.Add(this.btnDeleteStation);
            this.Controls.Add(this.btnEditStation);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.btnSaveStation);
            this.Controls.Add(this.txtEstation);
            this.Controls.Add(this.label1);
            this.Name = "StationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estación";
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tpTerminals.ResumeLayout(false);
            this.tpTerminals.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTerminals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.tpContacts.ResumeLayout(false);
            this.tpContacts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.tpServers.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEstation;
        private System.Windows.Forms.Button btnSaveStation;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tpTerminals;
        private DevExpress.XtraTab.XtraTabPage tpContacts;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraGrid.GridControl gcTerminals;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.Button btnAddTerminal;
        private System.Windows.Forms.TextBox txtPos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNumSerie;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddContacts;
        private DevExpress.XtraGrid.GridControl gcContacts;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.MaskedTextBox txtPhone;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtContactName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnEditStation;
        private System.Windows.Forms.Button btnDeleteStation;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.Button btnTerminal;
        private DevExpress.XtraTab.XtraTabPage tpServers;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox txtLealtadPort;
        private System.Windows.Forms.MaskedTextBox txtLealtadIP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MaskedTextBox txtGPRSIP;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox txtGPRSPort;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.MaskedTextBox txtMonederoIP;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox txtMonederoPort;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MaskedTextBox txtEfectivosPort;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox txtEfectivosIP;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox txtValesIP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox txtValesPort;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnSaveServers;
        private System.Windows.Forms.Button btnTicket;
    }
}