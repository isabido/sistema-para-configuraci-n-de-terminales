﻿using AppConfigTerminal;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LibUtil;
using LibUtil.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppConfigTerminal2
{
    public partial class StationForm : Form
    {
        private Station station = null;
        private List<Terminal> terminals = null;
        private List<Contact> contacts = null;
        private Group group = null;
        private BindingList<Terminal> grid_terminals = null;
        private BindingList<Contact> grid_contacts = null;
        private bool band = false;

        public StationForm(Group group)
        {
            InitializeComponent();
            this.group = group;
            this.Text = "Grupo: " + group.Name;
            station = new Station();
            station.GroupId = group.Id;
            grid_terminals = new BindingList<Terminal>();
            gcTerminals.DataSource = grid_terminals;
            grid_terminals = new BindingList<Terminal>();
            gcTerminals.DataSource = grid_terminals;
            this.Text = "Grupo: " + this.group.Name;

        }

        public StationForm(Group group, int stationId)
        {
            InitializeComponent();
            station = new Station();
            this.group = group;
            station.GroupId = this.group.Id;

            this.station = station.getStation(stationId);
            if(this.station != null && this.station.Id != 0)
            {

                grid_terminals = new BindingList<Terminal>(this.station.Terminals);
                grid_contacts = new BindingList<Contact>(this.station.Contacts);
                this.fill();
                this.fillIPs();
                this.Text = "Grupo: " + this.group.Name + " - Estación: " + this.station.Number;
                btnTicket.Enabled = true;
            }
            else
            {
                station = new Station();
                station.GroupId = group.Id;
                grid_terminals = new BindingList<Terminal>();
                grid_contacts = new BindingList<Contact>();
                this.enableControls();
                this.Text = "Grupo: " + this.group.Name;
            }
            gcTerminals.DataSource = grid_terminals;

            gcContacts.DataSource = grid_contacts;
            gridView1.Columns["StationId"].Visible = false;
            gridView1.Columns["Id"].Visible = false;
            

        }

        private void fill()
        {
            txtEstation.Enabled = false;
            txtEstation.Text = this.station.Number;
            tpTerminals.PageEnabled = true;
            tpContacts.PageEnabled = true;
            gridView1.OptionsBehavior.Editable = false;
            gridView2.OptionsBehavior.Editable = false;
            disableControls();
            this.band = false;
            
        }

        private void btnAddTerminal_Click(object sender, EventArgs e)
        {
            Terminal t = new Terminal();
            t.NumSerie = txtNumSerie.Text.Trim();
            t.Model = txtModel.Text.Trim();
            t.POS = txtPos.Text.Trim();
            t.StationId = this.station.Id;
            this.resetTerminals();
            this.grid_terminals.Add(t);
            
        }

        private void txtEstation_KeyUp(object sender, KeyEventArgs e)
        {
            if(txtEstation.Text.Trim() != String.Empty)
            {
                station.Number = txtEstation.Text.Trim();
                //station.GroupId = group.Id;
                tpTerminals.PageEnabled = true;
                tpContacts.PageEnabled = true;
            }
            else
            {
                tpTerminals.PageEnabled = false;
                tpContacts.PageEnabled = false;
                this.grid_terminals = null;
                this.grid_contacts = null;
                this.terminals = null;
                this.contacts = null;
                this.station.Number = String.Empty;
            }
            
        }

        private void resetTerminals()
        {
            txtModel.Text = String.Empty;
            txtNumSerie.Text = String.Empty;
            txtPos.Text = String.Empty;
            btnAddTerminal.Enabled = false;
        }

        private void resetContacts()
        {
            txtContactName.Text = String.Empty;
            txtEmail.Text = String.Empty;
            txtPhone.Text = String.Empty;
            btnAddContacts.Enabled = false;
        }

        private void txtModel_KeyUp(object sender, KeyEventArgs e)
        {
            this.verifyTextTerminals();
        }

        private void txtPos_KeyUp(object sender, KeyEventArgs e)
        {
            this.verifyTextTerminals();
        }

        private void verifyTextTerminals()
        {
            if (txtModel.Text.Trim() != String.Empty && txtPos.Text.Trim() != String.Empty)
            {
                btnAddTerminal.Enabled = true;
            }
            else
            {
                btnAddTerminal.Enabled = false;
            }
        }

        private void verifyTextContacs()
        {
            if(txtContactName.Text.Trim() != String.Empty && txtPhone.Text.Trim() != String.Empty)
            {
                btnAddContacts.Enabled = true;
            }
            else
            {
                btnAddContacts.Enabled = false;
            }
        }

        private void txtModel_Leave(object sender, EventArgs e)
        {
            this.verifyTextTerminals();
        }

        private void txtPos_Leave(object sender, EventArgs e)
        {
            this.verifyTextTerminals();
        }

        private void txtNumSerie_Leave(object sender, EventArgs e)
        {
            this.verifyTextTerminals();
        }

        private void txtContactName_KeyUp(object sender, KeyEventArgs e)
        {
            this.verifyTextContacs();
        }

        private void txtContactName_Leave(object sender, EventArgs e)
        {
            this.verifyTextContacs();
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            this.verifyTextContacs();
        }

        private void txtPhone_KeyUp(object sender, KeyEventArgs e)
        {
            this.verifyTextContacs();
        }

        private void txtPhone_Leave(object sender, EventArgs e)
        {
            this.verifyTextContacs();
        }

        private void btnAddContacts_Click(object sender, EventArgs e)
        {
            Contact c = new Contact();
            c.Name = txtContactName.Text.Trim();
            c.Phone = txtPhone.Text.Trim();
            c.Email = txtEmail.Text.Trim();
            c.StationId = this.station.Id;
            this.resetContacts();
            grid_contacts.Add(c);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(grid_contacts != null)
            {
                this.station.Contacts = grid_contacts.ToList<Contact>();
                this.station.Terminals = grid_terminals.ToList<Terminal>();
            }
            
            this.Text = "Grupo: " + this.group.Name + " - Estación: " + this.station.Number;
            if (!this.station.insert())
            {
                var x = this.station.Id;
                MessageBox.Show("Se ha actualizado la información de la estación.");
                btnTicket.Enabled = true;
            }
            else
            {
                var x = this.station.Id;
                MessageBox.Show("Se han agregado los datos de la estación.");
                btnTicket.Enabled = true;
            }
            
        }

        private void gcContacts_DoubleClick(object sender, EventArgs e)
        {
           
        }

        private void DoRowDoubleClickContact(GridView view, Point pt)
        {
            GridHitInfo info = view.CalcHitInfo(pt);
            if (info.InRow || info.InRowCell)
            {
                string colCaption = info.Column == null ? "N/A" : info.Column.GetCaption();
                var x = grid_contacts.ElementAt(info.RowHandle);
                

                DialogResult result1 = MessageBox.Show("¿Desea eliminar al siguiente contacto: " + x.Name + "?", "Eliminar Contacto", MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    Contact c = new Contact();
                    if (c.deleteContact(x.Id))
                        grid_contacts.RemoveAt(info.RowHandle);
                    else
                        MessageBox.Show("No se ha podido eliminar el contacto: {0}", x.Name);
                }
            }

            
        }

        private void DoRowDoubleClickTerminal(GridView view, Point pt)
        {
            GridHitInfo info = view.CalcHitInfo(pt);
            if (info.InRow || info.InRowCell)
            {
                string colCaption = info.Column == null ? "N/A" : info.Column.GetCaption();
                var x = grid_terminals.ElementAt(info.RowHandle);


                DialogResult result1 = MessageBox.Show("¿Desea eliminar la siguiente terminal con numero de serie: " + x.NumSerie + "?", "Eliminar Terminal", MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    Terminal t = new Terminal();
                    if (t.deleteTerminal(x.Id))
                        grid_terminals.RemoveAt(info.RowHandle);
                    else
                        MessageBox.Show("No se ha podido eliminar la terminal: {0}", x.Model);
                }
            }


        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            if (band)
            {
                GridView view = (GridView)sender;
                Point pt = view.GridControl.PointToClient(Control.MousePosition);
                DoRowDoubleClickContact(view, pt);
            }
            
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (band)
            {
                GridView view = (GridView)sender;
                Point pt = view.GridControl.PointToClient(Control.MousePosition);
                DoRowDoubleClickTerminal(view, pt);
            }
            
        }

        private void enableControls()
        {
            txtEstation.Enabled = true;
            btnSaveStation.Enabled = true;
            btnDeleteStation.Enabled = true;
            btnEditStation.Enabled = false;
            txtContactName.Enabled = true;
            txtNumSerie.Enabled = true;
            txtModel.Enabled = true;
            txtEmail.Enabled = true;
            txtPhone.Enabled = true;
            txtPos.Enabled = true;
            if (this.station.Id != 0)
                btnTicket.Enabled = true;

        }

        private void disableControls()
        {
            txtEstation.Enabled = false;
            btnSaveStation.Enabled = false;
            btnDeleteStation.Enabled = true;
            btnEditStation.Enabled = true;
            txtContactName.Enabled = false;
            txtNumSerie.Enabled = false;
            txtModel.Enabled = false;
            txtEmail.Enabled = false;
            txtPhone.Enabled = false;
            txtPos.Enabled = false;
        }

        private void btnEditStation_Click(object sender, EventArgs e)
        {
            txtEstation.Enabled = true;
            if(txtEstation.Text.Trim() != String.Empty)
            {
                xtraTabControl1.Enabled = true;
                gcContacts.Enabled = true;
                gcTerminals.Enabled = true;
                gridView1.OptionsBehavior.Editable = true;
                gridView2.OptionsBehavior.Editable = true;
                enableControls();
                this.band = true;
                btnDeleteStation.Enabled = true;
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            this.resetStation();
        }

        public void resetStation()
        {
            station = new Station();
            station.GroupId = 1;
            grid_terminals = new BindingList<Terminal>();
            grid_contacts = new BindingList<Contact>();
            this.enableControls();
            txtEstation.Text = String.Empty;
            gcContacts.DataSource = grid_contacts;
            gcTerminals.DataSource = grid_terminals;
            gridView1.Columns["StationId"].Visible = false;
            gridView1.Columns["Id"].Visible = false;
            tpTerminals.PageEnabled = false;
            tpContacts.PageEnabled = false;
            txtEfectivosIP.Text = String.Empty;
            txtLealtadIP.Text = String.Empty;
            txtMonederoIP.Text = String.Empty;
            txtGPRSIP.Text = String.Empty;
            txtValesIP.Text = String.Empty;

            txtEfectivosPort.Text = String.Empty;
            txtLealtadPort.Text = String.Empty;
            txtMonederoPort.Text = String.Empty;
            txtGPRSPort.Text = String.Empty;
            txtValesPort.Text = String.Empty;

        }

        private void btnTerminal_Click(object sender, EventArgs e)
        {
            FrmFunNumeric funcNum = new FrmFunNumeric(this.station.Id);
            funcNum.ShowDialog();
        }

        private void btnDeleteStation_Click(object sender, EventArgs e)
        {
            if(this.station.Id != 0)
            {
                if (this.station.deleteStation(this.station.Id))
                {
                    MessageBox.Show("Se ha eliminado la estación.");
                    this.resetStation();
                }
            }
        }

        
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        public void fillIPs()
        {
            if (this.station.Ips_list.Count != 0)
            {
                txtValesPort.Text = this.station.Ips_list.ElementAt(0).Port.ToString();
                txtEfectivosPort.Text = this.station.Ips_list.ElementAt(1).Port.ToString();
                txtMonederoPort.Text = this.station.Ips_list.ElementAt(2).Port.ToString();
                txtGPRSPort.Text = this.station.Ips_list.ElementAt(3).Port.ToString();
                txtLealtadPort.Text = this.station.Ips_list.ElementAt(4).Port.ToString();

                txtValesIP.Text = this.station.Ips_list.ElementAt(0).IP;
                txtEfectivosIP.Text = this.station.Ips_list.ElementAt(1).IP;
                txtMonederoIP.Text = this.station.Ips_list.ElementAt(2).IP;
                txtGPRSIP.Text = this.station.Ips_list.ElementAt(3).IP;
                txtLealtadIP.Text = this.station.Ips_list.ElementAt(4).IP;
            }
            

        }

        private void btnSaveServers_Click(object sender, EventArgs e)
        {

                IPs ip_vales = new IPs();
                IPs ip_efectivo = new IPs();
                IPs ip_monedero = new IPs();
                IPs ip_gprs = new IPs();
                IPs ip_lealtad = new IPs();
            if(this.station.Id != 0)
            {
                if (this.station.Ips_list.Count == 0)
                {
                    this.station.Ips_list.Add(ip_vales);
                    this.station.Ips_list.Add(ip_efectivo);
                    this.station.Ips_list.Add(ip_monedero);
                    this.station.Ips_list.Add(ip_gprs);
                    this.station.Ips_list.Add(ip_lealtad);


                }

                try
                {
                    this.station.Ips_list.ElementAt(0).Port = Int32.Parse(txtValesPort.Text.Trim());
                    this.station.Ips_list.ElementAt(1).Port = Int32.Parse(txtEfectivosPort.Text.Trim());
                    this.station.Ips_list.ElementAt(2).Port = Int32.Parse(txtMonederoPort.Text.Trim());
                    this.station.Ips_list.ElementAt(3).Port = Int32.Parse(txtGPRSPort.Text.Trim());
                    this.station.Ips_list.ElementAt(4).Port = Int32.Parse(txtLealtadPort.Text.Trim());

                    this.station.Ips_list.ElementAt(0).IP = txtValesIP.Text.Trim();
                    this.station.Ips_list.ElementAt(0).Server = "Servidor Vales";
                    this.station.Ips_list.ElementAt(0).StationId = station.Id;

                    this.station.Ips_list.ElementAt(1).IP = txtEfectivosIP.Text.Trim();
                    this.station.Ips_list.ElementAt(1).Server = "Servidor Efectivo";
                    this.station.Ips_list.ElementAt(1).StationId = station.Id;

                    this.station.Ips_list.ElementAt(2).IP = txtMonederoIP.Text.Trim();
                    this.station.Ips_list.ElementAt(2).Server = "Servidor Monedero";
                    this.station.Ips_list.ElementAt(2).StationId = station.Id;

                    this.station.Ips_list.ElementAt(3).IP = txtGPRSIP.Text.Trim();
                    this.station.Ips_list.ElementAt(3).Server = "Servidor GPRS";
                    this.station.Ips_list.ElementAt(3).StationId = station.Id;

                    this.station.Ips_list.ElementAt(4).IP = txtLealtadIP.Text.Trim();
                    this.station.Ips_list.ElementAt(4).Server = "Servidor Lealtad";
                    this.station.Ips_list.ElementAt(4).StationId = station.Id;

                    this.station.insertIP();
                    var x = this.station.Ips_list;
                    var band = false;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Los puertos deben ser un valor numérico entero.");
                }
            }
            else
            {
                MessageBox.Show("Primero se debe crear una estación para asignarle las direcciones de los servidores.");
            }
                
            

            

        }

        private void btnTicket_Click(object sender, EventArgs e)
        {
            TicketForm ticketF = new TicketForm(this.station.Id);
            ticketF.ShowDialog();
        }
    }
}
