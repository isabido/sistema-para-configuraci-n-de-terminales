﻿namespace AppConfigTerminal2
{
    partial class TicketForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtHead1 = new System.Windows.Forms.TextBox();
            this.txtHead2 = new System.Windows.Forms.TextBox();
            this.txtRs1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRs2 = new System.Windows.Forms.TextBox();
            this.txtRs3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtDF3 = new System.Windows.Forms.TextBox();
            this.txtDF2 = new System.Windows.Forms.TextBox();
            this.txtDF1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRFC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSIIC = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDS3 = new System.Windows.Forms.TextBox();
            this.txtDS2 = new System.Windows.Forms.TextBox();
            this.txtDS1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblHead1 = new System.Windows.Forms.Label();
            this.lblHead2 = new System.Windows.Forms.Label();
            this.lblUrl = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Encabezados";
            // 
            // txtHead1
            // 
            this.txtHead1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHead1.Location = new System.Drawing.Point(34, 38);
            this.txtHead1.MaxLength = 39;
            this.txtHead1.Name = "txtHead1";
            this.txtHead1.Size = new System.Drawing.Size(297, 21);
            this.txtHead1.TabIndex = 1;
            this.txtHead1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtHead1_KeyUp);
            this.txtHead1.Leave += new System.EventHandler(this.txtHead1_Leave);
            // 
            // txtHead2
            // 
            this.txtHead2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHead2.Location = new System.Drawing.Point(34, 61);
            this.txtHead2.MaxLength = 39;
            this.txtHead2.Name = "txtHead2";
            this.txtHead2.Size = new System.Drawing.Size(297, 21);
            this.txtHead2.TabIndex = 2;
            this.txtHead2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtHead2_KeyUp);
            this.txtHead2.Leave += new System.EventHandler(this.txtHead2_Leave);
            // 
            // txtRs1
            // 
            this.txtRs1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRs1.Location = new System.Drawing.Point(37, 108);
            this.txtRs1.MaxLength = 39;
            this.txtRs1.Name = "txtRs1";
            this.txtRs1.Size = new System.Drawing.Size(294, 21);
            this.txtRs1.TabIndex = 18;
            this.txtRs1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRs1_KeyUp);
            this.txtRs1.Leave += new System.EventHandler(this.txtRs1_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(34, 87);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 15);
            this.label9.TabIndex = 17;
            this.label9.Text = "Razón Social";
            // 
            // txtRs2
            // 
            this.txtRs2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRs2.Location = new System.Drawing.Point(37, 132);
            this.txtRs2.MaxLength = 39;
            this.txtRs2.Name = "txtRs2";
            this.txtRs2.Size = new System.Drawing.Size(294, 21);
            this.txtRs2.TabIndex = 19;
            this.txtRs2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRs2_KeyUp);
            this.txtRs2.Leave += new System.EventHandler(this.txtRs2_Leave);
            // 
            // txtRs3
            // 
            this.txtRs3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRs3.Location = new System.Drawing.Point(37, 155);
            this.txtRs3.MaxLength = 39;
            this.txtRs3.Name = "txtRs3";
            this.txtRs3.Size = new System.Drawing.Size(294, 21);
            this.txtRs3.TabIndex = 20;
            this.txtRs3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRs3_KeyUp);
            this.txtRs3.Leave += new System.EventHandler(this.txtRs3_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 15);
            this.label2.TabIndex = 21;
            this.label2.Text = "Nombre (Comercio)";
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(37, 198);
            this.txtName.MaxLength = 39;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(294, 21);
            this.txtName.TabIndex = 22;
            this.txtName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // txtDF3
            // 
            this.txtDF3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDF3.Location = new System.Drawing.Point(39, 291);
            this.txtDF3.MaxLength = 39;
            this.txtDF3.Name = "txtDF3";
            this.txtDF3.Size = new System.Drawing.Size(294, 21);
            this.txtDF3.TabIndex = 26;
            this.txtDF3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDF3_KeyUp);
            // 
            // txtDF2
            // 
            this.txtDF2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDF2.Location = new System.Drawing.Point(39, 268);
            this.txtDF2.MaxLength = 39;
            this.txtDF2.Name = "txtDF2";
            this.txtDF2.Size = new System.Drawing.Size(294, 21);
            this.txtDF2.TabIndex = 25;
            this.txtDF2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDF2_KeyUp);
            // 
            // txtDF1
            // 
            this.txtDF1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDF1.Location = new System.Drawing.Point(39, 244);
            this.txtDF1.MaxLength = 39;
            this.txtDF1.Name = "txtDF1";
            this.txtDF1.Size = new System.Drawing.Size(294, 21);
            this.txtDF1.TabIndex = 24;
            this.txtDF1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDF1_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 223);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 15);
            this.label3.TabIndex = 23;
            this.label3.Text = "Dirección Fiscal";
            // 
            // txtRFC
            // 
            this.txtRFC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRFC.Location = new System.Drawing.Point(39, 337);
            this.txtRFC.MaxLength = 39;
            this.txtRFC.Name = "txtRFC";
            this.txtRFC.Size = new System.Drawing.Size(294, 21);
            this.txtRFC.TabIndex = 28;
            this.txtRFC.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRFC_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 316);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 15);
            this.label4.TabIndex = 27;
            this.label4.Text = "RFC";
            // 
            // txtSIIC
            // 
            this.txtSIIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSIIC.Location = new System.Drawing.Point(39, 383);
            this.txtSIIC.MaxLength = 39;
            this.txtSIIC.Name = "txtSIIC";
            this.txtSIIC.Size = new System.Drawing.Size(294, 21);
            this.txtSIIC.TabIndex = 30;
            this.txtSIIC.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSIIC_KeyUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(36, 362);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 15);
            this.label5.TabIndex = 29;
            this.label5.Text = "SIIC";
            // 
            // txtDS3
            // 
            this.txtDS3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDS3.Location = new System.Drawing.Point(39, 476);
            this.txtDS3.MaxLength = 39;
            this.txtDS3.Name = "txtDS3";
            this.txtDS3.Size = new System.Drawing.Size(294, 21);
            this.txtDS3.TabIndex = 34;
            this.txtDS3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDS3_KeyUp);
            // 
            // txtDS2
            // 
            this.txtDS2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDS2.Location = new System.Drawing.Point(39, 453);
            this.txtDS2.MaxLength = 39;
            this.txtDS2.Name = "txtDS2";
            this.txtDS2.Size = new System.Drawing.Size(294, 21);
            this.txtDS2.TabIndex = 33;
            this.txtDS2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDS2_KeyUp);
            // 
            // txtDS1
            // 
            this.txtDS1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDS1.Location = new System.Drawing.Point(39, 429);
            this.txtDS1.MaxLength = 39;
            this.txtDS1.Name = "txtDS1";
            this.txtDS1.Size = new System.Drawing.Size(294, 21);
            this.txtDS1.TabIndex = 32;
            this.txtDS1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDS1_KeyUp);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(36, 408);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "Dirección Sucursal";
            // 
            // txtUrl
            // 
            this.txtUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUrl.Location = new System.Drawing.Point(37, 528);
            this.txtUrl.MaxLength = 39;
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(294, 21);
            this.txtUrl.TabIndex = 36;
            this.txtUrl.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtUrl_KeyUp);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(34, 507);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 15);
            this.label7.TabIndex = 35;
            this.label7.Text = "URL";
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImage = global::AppConfigTerminal2.Properties.Resources._1450392123_save;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Location = new System.Drawing.Point(282, 555);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(49, 49);
            this.btnSave.TabIndex = 37;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = global::AppConfigTerminal2.Properties.Resources.ticket_2;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.lblHead1);
            this.groupBox1.Controls.Add(this.lblHead2);
            this.groupBox1.Controls.Add(this.lblUrl);
            this.groupBox1.Location = new System.Drawing.Point(387, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 660);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // lblHead1
            // 
            this.lblHead1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHead1.AutoSize = true;
            this.lblHead1.BackColor = System.Drawing.Color.Transparent;
            this.lblHead1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead1.Location = new System.Drawing.Point(180, 56);
            this.lblHead1.Name = "lblHead1";
            this.lblHead1.Size = new System.Drawing.Size(0, 15);
            this.lblHead1.TabIndex = 17;
            this.lblHead1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHead1.SizeChanged += new System.EventHandler(this.lblHead1_SizeChanged_1);
            // 
            // lblHead2
            // 
            this.lblHead2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHead2.AutoSize = true;
            this.lblHead2.BackColor = System.Drawing.Color.Transparent;
            this.lblHead2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead2.Location = new System.Drawing.Point(180, 88);
            this.lblHead2.Name = "lblHead2";
            this.lblHead2.Size = new System.Drawing.Size(0, 15);
            this.lblHead2.TabIndex = 16;
            this.lblHead2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHead2.SizeChanged += new System.EventHandler(this.lblHead2_SizeChanged);
            // 
            // lblUrl
            // 
            this.lblUrl.BackColor = System.Drawing.Color.Transparent;
            this.lblUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUrl.Location = new System.Drawing.Point(27, 607);
            this.lblUrl.Name = "lblUrl";
            this.lblUrl.Size = new System.Drawing.Size(336, 20);
            this.lblUrl.TabIndex = 2;
            this.lblUrl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TicketForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 676);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtUrl);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtDS3);
            this.Controls.Add(this.txtDS2);
            this.Controls.Add(this.txtDS1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtSIIC);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtRFC);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDF3);
            this.Controls.Add(this.txtDF2);
            this.Controls.Add(this.txtDF1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtRs3);
            this.Controls.Add(this.txtRs2);
            this.Controls.Add(this.txtRs1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtHead2);
            this.Controls.Add(this.txtHead1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TicketForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TicketForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtHead1;
        private System.Windows.Forms.TextBox txtHead2;
        private System.Windows.Forms.TextBox txtRs1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtRs2;
        private System.Windows.Forms.TextBox txtRFC;
        private System.Windows.Forms.TextBox txtSIIC;
        private System.Windows.Forms.TextBox txtDS3;
        private System.Windows.Forms.TextBox txtDS2;
        private System.Windows.Forms.TextBox txtDS1;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDF1;
        private System.Windows.Forms.TextBox txtDF2;
        private System.Windows.Forms.TextBox txtDF3;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRs3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblHead1;
        private System.Windows.Forms.Label lblHead2;
        private System.Windows.Forms.Label lblUrl;
    }
}