﻿using LibUtil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibUtil.Models;
using System.Diagnostics;

namespace AppConfigTerminal2
{
    public partial class TicketForm : Form
    {
        private string head1 = String.Empty;
        private string head2 = String.Empty;
        private string lblHead = String.Empty;

        private string rs1 = String.Empty;
        private string rs2 = String.Empty;
        private string rs3 = String.Empty;
        private string strHead2 = String.Empty;

        private string nombre = String.Empty;

        private string df1 = String.Empty;
        private string df2 = String.Empty;
        private string df3 = String.Empty;

        private string rfc = String.Empty;
        private string siic = String.Empty;

        private string ds1 = String.Empty;
        private string ds2 = String.Empty;
        private string ds3 = String.Empty;
        
        private string url;

        private int MAX_LENGTH = 38;

        Ticket ticket = null;

        public TicketForm()
        {
            InitializeComponent();
            DbConnection db = new DbConnection();
            db.Init_DB();
            //NumericFunctions nf = new NumericFunctions();
            //var n = nf.getNF(2);
            //TerminalNumber tn = new TerminalNumber();
            //var t = tn.getTerminalNumbers();
            //this.ticket = new Ticket();
            //this.ticket = ticket.getTicket(1);
            //this.fillTicket();

        }

        public TicketForm(int stationId)
        {
            InitializeComponent();
            //DbConnection db = new DbConnection();
            //db.Init_DB();
            //NumericFunctions nf = new NumericFunctions();
            //var n = nf.getNF(2);
            //TerminalNumber tn = new TerminalNumber();
            //var t = tn.getTerminalNumbers();
            this.ticket = new Ticket();
            this.ticket = ticket.getTicket(stationId);
            if(this.ticket == null)
                this.ticket = new Ticket();
            this.fillTicket();
            this.ticket.StationId = stationId;

        }

        public void concat()
        {
            this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
        }

        private void txtHead1_KeyUp(object sender, KeyEventArgs e)
        {
            this.head1 = txtHead1.Text.Trim().ToUpper();
            if (this.verifyLength(this.head1))
            {
               
                if (this.head1 != String.Empty)
                {
                    //lblHead = this.head1;
                    this.head1 += Environment.NewLine;
                }
                else
                {
                    lblHead = String.Empty;
                }

                this.generateHead1();
            }else if (this.head1 != String.Empty)
            {
                this.head1 += Environment.NewLine;
                this.generateHead1();
            }
            
        }

        private void txtHead2_KeyUp(object sender, KeyEventArgs e)
        {
            this.head2 = txtHead2.Text.Trim().ToUpper();
            if (this.verifyLength(this.head2))
            {
                
                if (this.head2 != String.Empty)
                {
                    //this.lblHead += this.head2;
                    this.head2 += Environment.NewLine;
                }
                this.generateHead1();
                //lblHead1.Text = this.lblHead;
            }
            this.generateHead1();

        }

        
        private void txtHead1_Leave(object sender, EventArgs e)
        {
            this.generateHead1();
            //txtHead1.Text = this.head1;
            //lblHead1.Text = this.lblHead;
        }

        private void txtUrl_Leave(object sender, EventArgs e)
        {
            //txtUrl.Text = this.url;
        }

        private bool verifyLength(string text)
        {
            if (text.Length <= MAX_LENGTH) return true;

            MessageBox.Show("No pueden haber mas de "+ MAX_LENGTH.ToString() +" carácteres");
            return false;

        }

        private string substring(string text)
        {
            return text.Substring(0,MAX_LENGTH);
        }

        private void txtHead2_Leave(object sender, EventArgs e)
        {
            this.generateHead1();
            //txtHead2.Text = this.head2;
            //lblHead1.Text = this.lblHead;
        }

        private void lblHead2_SizeChanged(object sender, EventArgs e)
        {
            lblHead2.Left = (lblHead2.Parent.Width - lblHead2.Width) / 2;
        }

        private void lblHead1_SizeChanged_1(object sender, EventArgs e)
        {
            lblHead1.Left = (lblHead1.Parent.Width - lblHead1.Width) / 2;
        }

        private void txtRs1_KeyUp(object sender, KeyEventArgs e)
        {
            this.rs1 = txtRs1.Text.Trim().ToUpper();
            if (this.verifyLength(this.rs1))
            {
                if (this.rs1 != String.Empty)
                {
                    this.rs1 += Environment.NewLine;
                }
                this.generateHead2();
                    
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.rs1 != String.Empty)
            {
                this.rs1 += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtRs1_Leave(object sender, EventArgs e)
        {
            //txtRs1.Text = this.rs1;
            //lblHead2.Text = this.strHead2;
        }

        private void txtRs2_KeyUp(object sender, KeyEventArgs e)
        {
            this.rs2 = txtRs2.Text.Trim().ToUpper();
            if (this.verifyLength(this.rs2))
            {
                if(this.rs2 != String.Empty)
                {
                    this.rs2 += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.rs2 != String.Empty)
            {
                this.rs2 += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtRs2_Leave(object sender, EventArgs e)
        {
            //txtRs2.Text = this.rs2;
            //lblHead2.Text = this.strHead2;
        }

        private void txtRs3_KeyUp(object sender, KeyEventArgs e)
        {
            this.rs3 = txtRs3.Text.Trim().ToUpper();
            if (this.verifyLength(this.rs3))
            {
                if(this.rs3 != String.Empty)
                {
                    this.rs3 += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }
            else if (this.rs3 != String.Empty)
            {
                this.rs3 += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtRs3_Leave(object sender, EventArgs e)
        {
            //txtRs3.Text = this.rs3;
            //lblHead2.Text = this.strHead2;
        }

        public void generateHead1()
        {
            List<string> textbox = new List<string>();
            this.lblHead = String.Empty;
            this.lblHead = this.head1;
            this.lblHead += this.head2;
            
            lblHead1.Text = this.lblHead;

        }

        public void generateHead2()
        {
            this.strHead2 = String.Empty;
            this.strHead2 = this.rs1;
            this.strHead2 += this.rs2;
            this.strHead2 += this.rs3;
            this.strHead2 += this.nombre;
            this.strHead2 += this.df1;
            this.strHead2 += this.df2;
            this.strHead2 += this.df3;
            this.strHead2 += this.rfc;
            this.strHead2 += this.siic;
            this.strHead2 += this.ds1;
            this.strHead2 += this.ds2;
            this.strHead2 += this.ds3;
            lblHead2.Text = this.strHead2;
        }


        private void txtName_KeyUp(object sender, KeyEventArgs e)
        {
            this.nombre = txtName.Text.Trim().ToUpper();
            if (this.verifyLength(this.nombre))
            {
                if (this.nombre != String.Empty)
                {
                    this.nombre += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.nombre != String.Empty)
            {
                this.nombre += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtDF1_KeyUp(object sender, KeyEventArgs e)
        {
            this.df1 = txtDF1.Text.Trim().ToUpper();
            if (this.verifyLength(this.df1))
            {
                if (this.df1 != String.Empty)
                {
                    this.df1 += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.df1 != String.Empty)
            {
                this.df1 += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtDF2_KeyUp(object sender, KeyEventArgs e)
        {
            this.df2 = txtDF2.Text.Trim().ToUpper();
            if (this.verifyLength(this.df2))
            {
                if (this.df2 != String.Empty)
                {
                    this.df2 += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.df2 != String.Empty)
            {
                this.df2 += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtDF3_KeyUp(object sender, KeyEventArgs e)
        {
            this.df3 = txtDF3.Text.Trim().ToUpper();
            if (this.verifyLength(this.df3))
            {
                if (this.df3 != String.Empty)
                {
                    this.df3 += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.df3 != String.Empty)
            {
                this.df3 += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtRFC_KeyUp(object sender, KeyEventArgs e)
        {
            this.rfc = txtRFC.Text.Trim().ToUpper();
            if (this.verifyLength(this.rfc))
            {
                if (this.rfc != String.Empty)
                {
                    this.rfc += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.rfc != String.Empty)
            {
                this.rfc += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtSIIC_KeyUp(object sender, KeyEventArgs e)
        {
            this.siic = txtSIIC.Text.Trim().ToUpper();
            if (this.verifyLength(this.siic))
            {
                if (this.siic != String.Empty)
                {
                    this.siic += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.siic != String.Empty)
            {
                this.siic += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtDS1_KeyUp(object sender, KeyEventArgs e)
        {
            this.ds1 = txtDS1.Text.Trim().ToUpper();
            if (this.verifyLength(this.ds1))
            {
                if (this.ds1 != String.Empty)
                {
                    this.ds1 += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.ds1 != String.Empty)
            {
                this.ds1 += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtDS2_KeyUp(object sender, KeyEventArgs e)
        {
            this.ds2 = txtDS2.Text.Trim().ToUpper();
            if (this.verifyLength(this.ds1))
            {
                if (this.ds2 != String.Empty)
                {
                    this.ds2 += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.ds2 != String.Empty)
            {
                this.ds2 += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtDS3_KeyUp(object sender, KeyEventArgs e)
        {
            this.ds3 = txtDS3.Text.Trim().ToUpper();
            if (this.verifyLength(this.ds1))
            {
                if (this.ds3 != String.Empty)
                {
                    this.ds3 += Environment.NewLine;
                }
                this.generateHead2();
                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }
            else if (this.ds3 != String.Empty)
            {
                this.ds3 += Environment.NewLine;
                this.generateHead2();
            }
        }

        private void txtUrl_KeyUp(object sender, KeyEventArgs e)
        {
            this.url = txtUrl.Text.Trim().ToLower();
            if (this.verifyLength(this.url))
            {
                if (this.url != String.Empty)
                {
                    lblUrl.Text = this.url;
                }
                else
                {
                    lblUrl.Text = String.Empty;
                }

                //this.strHead2 = this.rs1 + Environment.NewLine + this.rs2 + Environment.NewLine + this.rs3;
                //lblHead2.Text = this.strHead2;
            }else if (this.url != String.Empty)
            {
                lblUrl.Text = this.url;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var band = false;
            if(lblHead != String.Empty && strHead2 != String.Empty)
            {
                //this.ticket = new Ticket();
                this.ticket.Head1 = this.head1;
                this.ticket.Head2 = this.head2;
                this.ticket.RazonSocial1 = this.rs1;
                this.ticket.RazonSocial2 = this.rs2;
                this.ticket.RazonSocial3 = this.rs3;
                this.ticket.NombreComercio = this.nombre;
                this.ticket.DireccionFiscal1 = this.df1;
                this.ticket.DireccionFiscal2 = this.df2;
                this.ticket.DireccionFiscal3 = this.df3;
                this.ticket.RFC = this.rfc;
                this.ticket.SIIC = this.siic;
                this.ticket.DireccionSucursal1 = this.ds1;
                this.ticket.DireccionSucursal2 = this.ds2;
                this.ticket.DireccionSucursal3 = this.ds3;
                this.ticket.Url = this.url;
                //this.ticket.StationId = 1;

                band = this.ticket.insertTicket();
                
            }

            if (band)
                MessageBox.Show("Se ha almacenado el ticket");
            else
                MessageBox.Show("Se han actualizado los datos del ticket");
        }

        public void fillTicket()
        {
            if(this.ticket != null)
            {
                this.head1 = this.ticket.Head1;
                this.head2 = this.ticket.Head2;
                this.rs1 = this.ticket.RazonSocial1;
                this.rs2 = this.ticket.RazonSocial2;
                this.rs3 = this.ticket.RazonSocial3;
                this.nombre = this.ticket.NombreComercio;
                this.df1 = this.ticket.DireccionFiscal1;
                this.df2 = this.ticket.DireccionFiscal2;
                this.df3 = this.ticket.DireccionFiscal3;
                this.rfc = this.ticket.RFC;
                this.siic = this.ticket.SIIC;
                this.ds1 = this.ticket.DireccionSucursal1;
                this.ds2 = this.ticket.DireccionSucursal2;
                this.ds3 = this.ticket.DireccionSucursal3;
                this.fillTextBox();
                this.generateHead1();
                this.generateHead2();
            }
            
        }

        public void fillStrTicket()
        {

        }

        public void fillTextBox()
        {
            this.txtHead1.Text= this.head1 != null ? this.head1.Trim() : String.Empty;
            this.txtHead2.Text = this.head2 != null ? this.head2.Trim() : String.Empty;
            this.txtRs1.Text = this.rs1!= null ? this.rs1.Trim(): String.Empty;
            this.txtRs2.Text = this.rs2!=null ? this.rs2.Trim() : String.Empty;
            this.txtRs3.Text = this.rs3 != null ? this.rs3.Trim() : String.Empty;

            this.txtName.Text = this.nombre != null ? this.nombre.Trim() : String.Empty;
            this.txtDF1.Text = this.df1 != null ? this.df1.Trim() : String.Empty;
            this.txtDF2.Text = this.df2 != null ? this.df2.Trim() : String.Empty;
            this.txtDF3.Text = this.df3 != null ? this.df3.Trim() : String.Empty;
            this.txtRFC.Text = this.rfc != null ? this.rfc.Trim() : String.Empty;
            this.txtSIIC.Text = this.siic != null ? this.siic.Trim() : String.Empty;
            this.txtDS1.Text = this.ds1 != null ? this.ds1.Trim() : String.Empty;
            this.txtDS2.Text = this.ds2 != null ? this.ds2.Trim() : String.Empty;
            this.txtDS3.Text = this.ds3 != null ? this.ds3.Trim() : String.Empty;
            this.txtUrl.Text = this.ds3 != null ? this.url.Trim() : String.Empty;
            
        }

    }
}
