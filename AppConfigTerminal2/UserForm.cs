﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibUtil;
using LibUtil.Models;
using LibUtil.utils;

namespace AppConfigTerminal2
{
    public partial class UserForm : Form
    {
        private User user = null;
        private string oldPass = String.Empty; 
        public UserForm()
        {
            InitializeComponent();
        }

        public UserForm(User user)
        {
            InitializeComponent();
            this.user = user;
            this.oldPass = user.Password;
            this.fillForm();

        }

        public void fillForm()
        {
            txtUser.Text = user.Nombre;

        }

        private void UserForm_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public void isNew()
        {
            string passwordRepeat = String.Empty;
            if (Utils.SHA512(txtPassword.Text.Trim()) != this.oldPass)
            {
                user.Password = txtPassword.Text.Trim();
                passwordRepeat = txtRepeat.Text.Trim();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string passwordRepeat = String.Empty;

            if(this.user == null)
            {
                this.user = new User();
                user.Password = Utils.SHA512(txtPassword.Text.Trim());
                
            }
            else
            {

                if(txtPassword.Text.Trim() != String.Empty && Utils.SHA512(txtPassword.Text.Trim()) != user.Password)
                {
                    user.Password = Utils.SHA512(txtPassword.Text.Trim());
                    passwordRepeat = Utils.SHA512(txtRepeat.Text.Trim());
                }
                else
                {
                    passwordRepeat = user.Password;
                }
            }
            
            user.Nombre = txtUser.Text.Trim();

            

            if (!Utils.isEmpty(user.Nombre) && !Utils.isEmpty(user.Password) && !Utils.isEmpty(passwordRepeat))
            {
                if (user.Password.Length >= 8)
                {
                    if (user.Password.Equals(passwordRepeat))
                    {
                        var u = user.create(user);
                        if (u != null)
                        {
                            MessageBox.Show(Messages.UserSaved);
                            DialogResult = System.Windows.Forms.DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show(Messages.ErrorSaving);
                        }
                        
                    }
                    else
                    {
                        MessageBox.Show(Messages.PasswordRepeat);
                    }
                }
                else
                {
                    MessageBox.Show(Messages.PasswordLengh);
                }
            }
            else
            {
                MessageBox.Show(Messages.EmptyString);
            }
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
