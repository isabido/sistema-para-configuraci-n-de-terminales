﻿using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using LibUtil.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppConfigTerminal2
{
    public partial class UserListForm : Form
    {
        private BindingList<User> users = null;
        private List<User> list_users = null;

        public UserListForm()
        {
            InitializeComponent();
            User u = new User();
            users = new BindingList<User>(u.getUsers());
            list_users = users.ToList<User>();
            gridControl1.DataSource = users;
            gridView1.Columns["Password"].OptionsColumn.AllowEdit = false;


        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            UserForm user = new UserForm();
            if (user.ShowDialog() == DialogResult.OK)
            {
                User u = new User();
                users = new BindingList<User>(u.getUsers());
                list_users = users.ToList<User>();
                gridControl1.DataSource = users;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            User u = new User();
            if(list_users == null)
                list_users = users.ToList<User>();
            u.insertUsers(list_users);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            Point pt = view.GridControl.PointToClient(Control.MousePosition);
            DoRowDoubleClickContact(view, pt);
        }

        private void DoRowDoubleClickContact(GridView view, Point pt)
        {
            GridHitInfo info = view.CalcHitInfo(pt);
            if (info.InRow || info.InRowCell)
            {
                string colCaption = info.Column == null ? "N/A" : info.Column.GetCaption();
                var x = users.ElementAt(info.RowHandle);

                UserForm user = new UserForm(x);
                if (user.ShowDialog() == DialogResult.OK)
                {
                    User u = new User();
                    users = new BindingList<User>(u.getUsers());
                    list_users = users.ToList<User>();
                    gridControl1.DataSource = users;
                }
            }


        }
    }
}
