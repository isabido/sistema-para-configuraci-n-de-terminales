﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Windows.Forms;
using LibUtil.Models;

namespace LibUtil
{
    public class ClsUtil
    {
       public string GetPath()
        {
            string path;
            path = System.IO.Path.GetFullPath(Application.StartupPath);
                //System.IO.Path.GetDirectoryName(
               //System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            return (path);
        }

        public int WriteDataXml(String NameFile,DataSet Data,String Path)
        {

            String Path1;
            Path1 = @String.Concat(Path, "\\", NameFile);
            Data.WriteXml(Path1, XmlWriteMode.WriteSchema);

            return 0;
        }


        public int ReadDataXml(String NameFile, DataSet Data, String Path)
        {

            Data.ReadXml(String.Concat(Path, "\\", NameFile));

            return 0;
        }


        public  DataSet FunctionABCD(String DtName,Char ValueA,Char ValueB, Char ValueC, Char ValueD)
        {
            DataTable dt = new DataTable(DtName);
            dt.Columns.Add("Button");
            dt.Columns.Add("Value");
            DataRow dr;

            dr = dt.NewRow();
            dr["Button"] = "A";
            dr["Value"] = ValueA;            
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Button"] = "B";
            dr["Value"] = ValueB;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Button"] = "C";
            dr["Value"] = ValueC;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Button"] = "D";
            dr["Value"] = ValueD;
            dt.Rows.Add(dr);

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            return ds;
        }

        //public DataSet FunctionNumeric(String DtName, Char Value1, Char Value2, Char Value3, Char Value4, Char Value5, Char Value6, Char Value7, Char Value8, Char Value9)
        //{
        //    DataTable dt = new DataTable(DtName);
        //    dt.Columns.Add("Button");
        //    dt.Columns.Add("Value");
        //    DataRow dr;

        //    dr = dt.NewRow();
        //    dr["Button"] = "E";
        //    dr["Value"] = Value1;
        //    dt.Rows.Add(dr);

        //    dr = dt.NewRow();
        //    dr["Button"] = "F";
        //    dr["Value"] = Value2;
        //    dt.Rows.Add(dr);

        //    dr = dt.NewRow();
        //    dr["Button"] = "G";
        //    dr["Value"] = Value3;
        //    dt.Rows.Add(dr);

        //    dr = dt.NewRow();
        //    dr["Button"] = "H";
        //    dr["Value"] = Value4;
        //    dt.Rows.Add(dr);

        //    dr = dt.NewRow();
        //    dr["Button"] = "I";
        //    dr["Value"] = Value5;
        //    dt.Rows.Add(dr);

        //    dr = dt.NewRow();
        //    dr["Button"] = "J";
        //    dr["Value"] = Value6;
        //    dt.Rows.Add(dr);

        //    dr = dt.NewRow();
        //    dr["Button"] = "K";
        //    dr["Value"] = Value7;
        //    dt.Rows.Add(dr);

        //    dr = dt.NewRow();
        //    dr["Button"] = "L";
        //    dr["Value"] = Value8;
        //    dt.Rows.Add(dr);

        //    dr = dt.NewRow();
        //    dr["Button"] = "M";
        //    dr["Value"] = Value9;
        //    dt.Rows.Add(dr);
        //    DataSet ds = new DataSet();
        //    ds.Tables.Add(dt);

        //    return ds;
        //}

        public List<DropValues> FunctionNumeric(List<string> combovalues)
        {
            var list = new DropValues();
            var bdValues = list.getTerminalNumbers();
            var listValues = new List<DropValues>();
            foreach(var c in combovalues)
            {
                var b = bdValues.Find(x => x.Value.Contains(c));
                if (b != null)
                    listValues.Add(b);
            }

            return listValues;
        }

        public static DataSet FunctionCreateDropValues()
        {
            String DtName;
            DtName = "ValuesDrop";
            DataTable dt = new DataTable(DtName);
            dt.Columns.Add("Value");
            dt.Columns.Add("Text");
            DataRow dr;

            dr = dt.NewRow();
            dr["Value"] = "A";
            dr["Text"] = "EFECTIVO SE";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "B";
            dr["Text"] = "CREDITOS SE";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "C";
            dr["Text"] = "PUNTOGAS VOLUMETRICO";            
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "D";
            dr["Text"] = "PUNTOGAS TRADICIONAL";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "E";
            dr["Text"] = "FACTURACION SE";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "F";
            dr["Text"] = "SIN ASIGNAR";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "G";
            dr["Text"] = "ADMINISTRATIVO";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "H";
            dr["Text"] = "TECNICO";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "I";
            dr["Text"] = "NETWORK CONTROL PANEL";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "J";
            dr["Text"] = "CONEXIONES";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "K";
            dr["Text"] = "PUNTOGAS TRAD: VENTA";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "L";
            dr["Text"] = "CANCELAR";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "M";
            dr["Text"] = "PUNTOGAS VOL: VENTA";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "N";
            dr["Text"] = "PUNTOGAS VOL: IMPRESION TICKET";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "O";
            dr["Text"] = "SIN ASIGNAR";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "P";
            dr["Text"] = "PUNTOGAS VOL: LIMPIAR";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "Q";
            dr["Text"] = "EFECTIVO GASOMARSHAL";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "R";
            dr["Text"] = "FACTURACION GASOMARSHAL";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "S";
            dr["Text"] = "VALES SE";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "T";
            dr["Text"] = "PUNTOGAS: RECUPERA TICKET";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "U";
            dr["Text"] = "VALES: GASOMARSHAL";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "V";
            dr["Text"] = "LEALTAD EMPLEADOS";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = "0";
            dr["Text"] = "EMPTY";
            dt.Rows.Add(dr);


            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            return ds;
        }

        public DataSet FunctionABCDCreateDropValues(String DtName, Char ValueA, Char ValueB, Char ValueC, Char ValueD)
        {
            DataTable dt = new DataTable(DtName);
            dt.Columns.Add("Value");
            dt.Columns.Add("Text");
            DataRow dr;

            dr = dt.NewRow();
            dr["Text"] = "A";
            dr["Value"] = 1;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Text"] = "B";
            dr["Value"] = 2;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Text"] = "C";
            dr["Value"] = 3;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Text"] = "D";
            dr["Value"] = 4;
            dt.Rows.Add(dr);

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            return ds;
        }

        public static DataSet FunctionABCDCreateDropValues()
        {
            String DtName;
            DtName = "ValuesDrop";
            DataTable dt = new DataTable(DtName);
            dt.Columns.Add("Value");
            dt.Columns.Add("Text");
            DataRow dr;

            dr = dt.NewRow();
            dr["Value"] = 1;
            dr["Text"] = "E";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = 2;
            dr["Text"] = "F";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = 3;
            dr["Text"] = "G";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = 4;
            dr["Text"] = "H";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = 5;
            dr["Text"] = "I";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = 6;
            dr["Text"] = "J";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = 7;
            dr["Text"] = "K";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = 8;
            dr["Text"] = "L";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Value"] = 9;
            dr["Text"] = "M";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Text"] = "A";
            dr["Value"] = 10;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Text"] = "B";
            dr["Value"] = 11;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Text"] = "C";
            dr["Value"] = 12;
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Text"] = "D";
            dr["Value"] = 13;
            dt.Rows.Add(dr);

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            return ds;
        }

        public string FunctionTicket(string letter, int ticketId)
        {
            Ticket ticket = new Ticket();
            return ticket.getTicketColumn(ticketId, letter);
        } 

        //Elimina la Base de datos del dropdown
        public void CreateDBDropDown(String NameDropDB)
        {
                WriteDataXml(NameDropDB, FunctionCreateDropValues(), String.Concat(GetPath()));
        
        }

        //borra la base de datos que llena el dropdown
        public void DeleteDBDropDown(String FullPathFile)
        {                       
            File.Delete(FullPathFile);
         
        }

        public Boolean FileExist(String FileName)
        {
            Boolean Value;
            Value = false;

            if (File.Exists(FileName))
            {
                Value = true;
            }

            return Value;

        }


    }
}
