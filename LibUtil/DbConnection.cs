﻿using LibUtil.Models;
using NLog;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibUtil
{
    public class Config
    {
        public static string SqliteMemoryDb = ":memory:";
        public static string SqliteFileDb = Directory.GetCurrentDirectory() + @"\mydb.db";
    }

    public class DbConnection
    {
       
        string path = Directory.GetCurrentDirectory() + @"\mydb.db";
        private OrmLiteConnectionFactory DbFactory = null;
        public static Logger logger = LogManager.GetCurrentClassLogger();

        protected void CreateNewDatabase()
        {
            using (var db = OpenDbConnection())
            {
                db.DropAndCreateTable<User>();
                db.DropAndCreateTable<Group>();
                db.DropAndCreateTable<Station>();
                db.DropAndCreateTable<Contact>();
                db.DropAndCreateTable<DropValues>();
                db.DropAndCreateTable<TerminalNumber>();
                db.DropAndCreateTable<NumericFunctions>();
                db.DropAndCreateTable<Ticket>();
                db.DropAndCreateTable<Terminal>();
                db.DropAndCreateTable<IPs>();

            }

        }

        protected void CreateDatabase()
        {
            using (var db = OpenDbConnection())
            {
                db.CreateTable<User>();
                db.CreateTable<Group>();
                db.CreateTable<Station>();
                db.CreateTable<Contact>();
                db.CreateTable<DropValues>();
                db.CreateTable<TerminalNumber>();
                db.CreateTable<NumericFunctions>();
                db.CreateTable<Ticket>();
                db.CreateTable<Terminal>();
                db.CreateTable<IPs>();

                DropValues dv = new DropValues();
                dv.createValues();
                TerminalNumber tn = new TerminalNumber();
                tn.createNumericValues();
                User user = new User();
                user.defaultUser();

            }

        }

        public virtual IDbConnection OpenDbConnection()
        {
           if(DbFactory == null)
            {
                string connectionString = "Data Source=" + path + "Version=3;New=True";
                DbFactory = new OrmLiteConnectionFactory(path, SqliteDialect.Provider);
            }


            return DbFactory.OpenDbConnection();
        }

        public bool Init_DB()
        {
            try
            {
             
                using (var db = OpenDbConnection())
                {
                    CreateNewDatabase();
                    DropValues dv = new DropValues();
                    dv.createValues();
                    TerminalNumber tn = new TerminalNumber();
                    tn.createNumericValues();
                    User user = new User();
                    user.defaultUser();
                    return true;
                }
            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                Console.Out.WriteLine(e);
                return false;
            }

        }

        public bool First_DB()
        {
            try
            {
                if(!File.Exists(path)){
                    using (var db = OpenDbConnection())
                    {
                        CreateDatabase();
                        
                    }
                }
                return true;

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                Console.Out.WriteLine(e);
                return false;
            }

        }
    }
}
