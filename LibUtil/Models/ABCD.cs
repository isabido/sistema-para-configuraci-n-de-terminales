﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;
using System.Data;

namespace LibUtil.Models
{
    public class ABCD
    {
        [AutoIncrement]
        public int Id { get; set; }
        public int Number { get; set; }
        public string Button { get; set; }

        public void createKeyValues()
        {
            var x = ClsUtil.FunctionABCDCreateDropValues();
            DataTable dt = x.Tables[0];
            DbConnection dbconnection = new DbConnection();
            using (var db = dbconnection.OpenDbConnection())
            {
                foreach (DataRow row in dt.Rows)
                {
                    db.Insert(new ABCD { Number = Convert.ToInt32(row["Value"]), Button = Convert.ToString(row["Text"]) });
                    string valor1 = Convert.ToString(row["Value"]);
                    string valor2 = Convert.ToString(row["Text"]);
                }
            }
        }

    }
}
