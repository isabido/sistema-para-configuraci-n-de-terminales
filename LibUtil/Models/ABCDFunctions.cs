﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;
using ServiceStack.DataAnnotations;

namespace LibUtil.Models
{
    class ABCDFunctions
    {
        [AutoIncrement]
        public int Id { get; set; }
        [Reference]
        public int TerminalNumberId { get; set; }
        [Reference]
        public int DropValuesId { get; set; }
        [Reference]
        public int StationId { get; set; }


    }
}
