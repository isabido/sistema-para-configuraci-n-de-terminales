﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;

namespace LibUtil.Models
{
    public class Contact
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        [ForeignKey(typeof(Station), OnDelete = "CASCADE", OnUpdate = "CASCADE")]
        public int StationId { get; set; }


        public bool insertContact()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    long contact = db.Insert(this, selectIdentity: true);
                    if (contact != 0) return true;
                    return false;

                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }

        public Contact getContact(int id)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    return db.SingleById<Contact>(id);
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

        public bool updateContact()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser actualiza el nombre del grupo
                    int band = db.Update<Contact>(new { Name = this.Name, Station = this.StationId, Email=this.Email, Phone=this.Phone }, p => p.Id == this.Id);
                    return band == 0 ? false : true;

                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }

        public bool deleteContact(int id)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    int band = db.Delete<Contact>(p => p.Id == id);
                    return band == 0 ? false : true;

                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }
    }
}
