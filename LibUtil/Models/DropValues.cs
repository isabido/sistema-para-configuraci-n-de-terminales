﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;

namespace LibUtil.Models
{
    public class DropValues
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }

        public void createValues()
        {
            var x = ClsUtil.FunctionCreateDropValues();
            DataTable dt = x.Tables[0];
            DbConnection dbconnection = new DbConnection();
            using (IDbConnection db = dbconnection.OpenDbConnection())
            {
                foreach (DataRow row in dt.Rows)
                {
                    db.Insert(new DropValues { Value = Convert.ToString(row["Value"]), Text = Convert.ToString(row["Text"]) });
                    string valor1 = Convert.ToString(row["Value"]);
                    string valor2 = Convert.ToString(row["Text"]);
                }
            }
        }

        public List<DropValues> getTerminalNumbers()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    return new List<DropValues>(db.Select<DropValues>());
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

    }
}
