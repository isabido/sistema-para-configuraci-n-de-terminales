﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibUtil.Models
{
    public class FullNumericFunctions
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public string StationNumber { get; set; }
        public int TerminalNumberNumber { get; set; }
        public string TerminalNumberButton { get; set; }
        public string DropValuesValue { get; set; }
        public string DropValuesText { get; set; }
    }
}
