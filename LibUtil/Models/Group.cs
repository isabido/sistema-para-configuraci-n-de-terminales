﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;
using System.ComponentModel;

namespace LibUtil.Models
{
    public class Group
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string Name{ get; set; }
        [Reference]
        public List<Station> Stations { get; set; }


        public BindingList<Group> getGroups()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    return new BindingList<Group>(db.LoadSelect<Group>());
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

        public List<Group> getGroups3()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    return new List<Group>(db.LoadSelect<Group>());
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }
        public Group getGroup(int id)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    return db.SingleById<Group>(id);
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

        public long insertGroup()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    long groupId = db.Insert(this, selectIdentity: true);
                    return groupId;

                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return 0;
            }
        }

        public bool deleteGroup(int id)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    int band = db.Delete<Group>(p => p.Id == id);
                    return band == 0? false:true;
                  
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }

        public bool updateGroup()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser actualiza el nombre del grupo
                    int band = db.Update<Group>(new { Name = this.Name}, p => p.Id == this.Id);
                    return band == 0 ? false : true;

                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }
    }
}
