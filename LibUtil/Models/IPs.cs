﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;

namespace LibUtil.Models
{
    public class IPs
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string Server { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }
        [Reference]
        public int StationId { get; set; }
        

        
    }
}
