﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;
using ServiceStack.DataAnnotations;

namespace LibUtil.Models
{
    public class NumericFunctions
    {
        [AutoIncrement]
        public int Id { get; set; }
        [Reference]
        public int TerminalNumberId { get; set; }
        [Reference]
        public int DropValuesId { get; set; }
        [Reference]
        public int StationId { get; set; }
       

        public List<FullNumericFunctions> getNF(int stationId)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //se obtienen los datos para la terminal
                    var d = db.Select<FullNumericFunctions, NumericFunctions>(q => q.Join<Station>().Join<NumericFunctions, TerminalNumber>((a,b)=>a.TerminalNumberId==b.Id).Join<NumericFunctions, DropValues>((nm,dv)=>nm.DropValuesId==dv.Id).Where(c=>c.StationId==stationId));
                    var rows = db.Select<FullNumericFunctions>(  // Map results to FullCustomerInfo POCO
                                  db.From<NumericFunctions>()                                       // Create typed Customer SqlExpression
                                    .LeftJoin<Station>()                            // Implict left join with base table
                                    .Join<NumericFunctions, TerminalNumber>((c, o) => c.TerminalNumberId == o.Id)
                                    .Join<NumericFunctions,DropValues>((nf,dv)=>nf.DropValuesId == dv.Id)
                                    .Where(c => c.StationId == stationId));
                    return rows;
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

        public List<NumericFunctions> getNF2(int stationId)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //se obtienen los datos para la terminal
                    //var d = db.Select<FullNumericFunctions, NumericFunctions>(q => q.Join<Station>().Join<NumericFunctions, TerminalNumber>((a,b)=>a.TerminalNumberId==b.Id).Join<NumericFunctions, DropValues>((nm,dv)=>nm.DropValuesId==dv.Id).Where(c=>c.StationId==stationId));
                    var rows = db.Select<NumericFunctions>(q=> q.StationId == stationId);
                    return rows;
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

        public List<NumericFunctions> insertNF(List<NumericFunctions> list_nf)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    foreach(var l in list_nf)
                    {
                        if(l != null && l.Id != 0)
                        {
                            db.Update(new NumericFunctions { Id = l.Id, TerminalNumberId = l.TerminalNumberId, DropValuesId =l.DropValuesId, StationId = l.StationId });
                        }
                        else
                        {
                            db.Insert<NumericFunctions>(new NumericFunctions { TerminalNumberId = l.TerminalNumberId, DropValuesId =l.DropValuesId, StationId=l.StationId });
                        }

                    }
                    return new List<NumericFunctions>(db.LoadSelect<NumericFunctions>());
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }


    }
}
