﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;

namespace LibUtil.Models
{
    public class Station
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string Number { get; set; }
        public string Client { get; set; }
        [Reference]
        public List<Contact> Contacts { get; set; }
        [Reference]
        public List<Terminal> Terminals { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Pos { get; set; }
        [ForeignKey(typeof(Group), OnDelete = "CASCADE", OnUpdate = "CASCADE")]
        public int GroupId { get; set; }
        [Reference]
        public List<IPs> Ips_list { get; set; }


        public List<Station> getStations(int groupId)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    return new List<Station>(db.LoadSelect<Station>(q => q.GroupId == groupId));
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

        public bool deleteStation(int id)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    int band = db.Delete<Station>(p=>p.Id == id);
                    return band == 0 ? false : true;

                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }

        public bool insert()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    bool band = db.Save(this, references: true);
                    //db.SaveAll(this.Terminals);
                    //db.SaveAll(this.Contacts);
                    return band;

                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }

        public Station getStation(int stationId)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {

                    return db.LoadSingleById<Station>(this.Id = stationId);
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

        public bool insertIP()
        {
            bool band = false;
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    IPs p = new IPs();
                    int x = db.SaveAll(this.Ips_list);
                    //bool band = db.Save(this);

                    return band;

                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }
    }

}
