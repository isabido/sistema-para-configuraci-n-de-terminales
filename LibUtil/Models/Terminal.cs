﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;
using ServiceStack.DataAnnotations;

namespace LibUtil.Models
{
    public class Terminal
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string POS { get; set; }
        public string NumSerie { get; set; }
        public string Model { get; set; }
        [ForeignKey(typeof(Station), OnDelete = "CASCADE", OnUpdate = "CASCADE")]
        public int StationId { get; set; }


        public bool deleteTerminal(int id)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    int band = db.Delete<Terminal>(p => p.Id == id);
                    return band == 0 ? false : true;

                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }

    }
}
