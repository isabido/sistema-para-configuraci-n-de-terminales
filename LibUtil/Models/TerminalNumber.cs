﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;
using ServiceStack.DataAnnotations;
using System.Data;

namespace LibUtil.Models
{
    public class TerminalNumber
    {
        [AutoIncrement]
        public int Id { get; set; }
        public int Number { get; set; }
        public string Button { get; set; }


        public void createNumericValues()
        {
            var x = ClsUtil.FunctionABCDCreateDropValues();
            DataTable dt = x.Tables[0];
            DbConnection dbconnection = new DbConnection();
            using (IDbConnection db = dbconnection.OpenDbConnection())
            {
                foreach (DataRow row in dt.Rows)
                {
                    db.Insert(new TerminalNumber { Number = Convert.ToInt32(row["Value"]), Button = Convert.ToString(row["Text"]) });
                    string valor1 = Convert.ToString(row["Value"]);
                    string valor2 = Convert.ToString(row["Text"]);
                }
            }
        }

        public List<TerminalNumber> getTerminalNumbers()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    return new List<TerminalNumber>(db.Select<TerminalNumber>());
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }
    }
}
