﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;

namespace LibUtil.Models
{
    public class Ticket
    {
        [AutoIncrement]
        public int Id { get; set; }
        public string Head1 { get; set; }
        public string Head2 { get; set; }
        public string RazonSocial1 { get; set; }
        public string RazonSocial2 { get; set; }
        public string RazonSocial3 { get; set; }
        public string NombreComercio { get; set; }
        public string DireccionFiscal1 { get; set; }
        public string DireccionFiscal2 { get; set; }
        public string DireccionFiscal3 { get; set; }
        public string RFC { get; set; }
        public string SIIC { get; set; }
        public string DireccionSucursal1 { get; set; }
        public string DireccionSucursal2 { get; set; }
        public string DireccionSucursal3 { get; set; }
        public string Url { get; set; }
        [Reference]
        public int? StationId { get; set; }

        public bool insertTicket()
        {
            long contact = 0;
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    return db.Save(this);
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }


        public Ticket getTicket(int stationId)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //ser verifica que se haya guardado el usuario en la bd
                    return db.SingleById<Ticket>(stationId);
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

        public string getTicketColumn(int ticketId, string letter)
        {
            
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    
                    switch (letter)
                    {
                        case "N":
                            
                            return db.Single<Ticket>(q => q.Id == ticketId).Head1;
                        case "O":

                            return db.Single<Ticket>(q => q.Id == ticketId).Head2;
                        case "P":

                            return db.Single<Ticket>(q => q.Id == ticketId).RazonSocial1;
                        case "Q":

                            return db.Single<Ticket>(q => q.Id == ticketId).RazonSocial2;
                        case "R":
                            return db.Single<Ticket>(q => q.Id == ticketId).RazonSocial3;
                        case "S":
                            return db.Single<Ticket>(q => q.Id == ticketId).NombreComercio;
                        case "T":
                            return db.Single<Ticket>(q => q.Id == ticketId).DireccionFiscal1;
                        case "U":
                            return db.Single<Ticket>(q => q.Id == ticketId).DireccionFiscal2;
                        case "V":
                            return db.Single<Ticket>(q => q.Id == ticketId).DireccionFiscal3;
                        case "W":
                            return db.Single<Ticket>(q => q.Id == ticketId).RFC;
                        case "X":
                            return db.Single<Ticket>(q => q.Id == ticketId).SIIC;
                        case "Y":
                            return db.Single<Ticket>(q => q.Id == ticketId).DireccionSucursal1;
                        case "Z":
                            return db.Single<Ticket>(q => q.Id == ticketId).DireccionSucursal2;
                        default:
                            return null;
                    }

                    //ser verifica que se haya guardado el usuario en la bd
                    //return db.SingleById<Ticket>(stationId);
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }
    }
}
