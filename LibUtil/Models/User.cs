﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;
using NLog;
using ServiceStack.DataAnnotations;
using LibUtil.utils;

namespace LibUtil.Models
{
    public class User
    {
        [AutoIncrement]
        public int Id { get; set; }
        [Index(Unique = true)]
        public string Nombre { get; set; }
        public string Password { get; set; }

        /*
        Se comprueba que el usuario exista en el sistema
            */
        public User login()
        {
            
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    User u = db.Single<User>(q => q.Nombre == Nombre && q.Password == Utils.SHA512(Password));
                    return u;
                }
            }
            catch(Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }

        }

        public List<User> getUsers()
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    var user = db.Select<User>();
                    //ser verifica que se haya guardado el usuario en la bd
                    return user;
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
        }

        public bool insertUsers(List<User> users)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    var user = db.SaveAll(users);
                    //ser verifica que se haya guardado el usuario en la bd
                    return true;
                }

            }
            catch (Exception e)
            {
                DbConnection.logger.Error(e);
                return false;
            }
        }

        public void defaultUser()
        {
            DbConnection dbconnection = new DbConnection();
            using (var db = dbconnection.OpenDbConnection())
            {
                db.Insert(new User { Nombre = "Admin", Password = Utils.SHA512("admin")});
            }
        }

        /*
        Se crea el usuario en la base de datos
        */
        public User create(User u)
        {
            try
            {
                DbConnection dbconnection = new DbConnection();
                using (var db = dbconnection.OpenDbConnection())
                {
                    //antes de guardar el password se cifra con un algoritmo de una vía
                    //u.Password = Utils.SHA512(u.Password);
                    db.Save(this);
                    
                    //ser verifica que se haya guardado el usuario en la bd
                    return db.Single<User>(new {id= this.Id, Nombre = u.Nombre });
                }
                
            }
            catch(Exception e)
            {
                DbConnection.logger.Error(e);
                return null;
            }
            
        }
    }
}
