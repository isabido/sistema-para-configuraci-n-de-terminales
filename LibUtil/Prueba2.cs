﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibUtil
{
    public class Prueba2
    {
        int id;
        string prueba;
        int pruebaId;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Prueba
        {
            get
            {
                return prueba;
            }

            set
            {
                prueba = value;
            }
        }

        public int PruebaId
        {
            get
            {
                return pruebaId;
            }

            set
            {
                pruebaId = value;
            }
        }
    }
}
