﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibUtil.utils
{
    public static class Messages
    {
        public static string PasswordRepeat = "Las contraseñas no coinciden";
        public static string PasswordLengh = "La longitud del password debe ser mayor o igual a 8 carácteres";
        public static string UserSaved = "Los datos del usuario se han guardado";

        public static string ErrorSaving = "Ha ocurrido un error y no se han podido almacenar los datos";
        public static string EmptyString = "Debes llenar los campos correctamente";
    }
}
