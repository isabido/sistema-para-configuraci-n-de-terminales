﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using LibUtil.Models;

namespace WindowsApplication3
{
    public class NestedRecord
    {
        private string fName;
        List<Station> fChildList = null;

        public NestedRecord(string name, List<Station> childList)
        {
            this.fName = name;
            this.fChildList = childList;
        }

        public string Name
        {
            get { return fName; }
            set { fName = value; }
        }

        public List<Station> ChildList
        {
            get { return fChildList; }
            set { fChildList = value; }
        }
    }
}
