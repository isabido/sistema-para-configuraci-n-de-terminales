﻿using LibUtil.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsApplication3
{
    public class NestedRecords : ArrayList
    {
        public NestedRecords(string a, List<Station> s)
        {
            Add(new NestedRecord(a, s));

        }

        public NestedRecords(List<Group> lista)
        {
            foreach (var l in lista)
            {
                Add(new NestedRecord(l.Name, l.Stations));
            }
            

        }

        public virtual new NestedRecord this[int index]
        {
            get { return (NestedRecord)(base[index]); }
        }
    }

}
